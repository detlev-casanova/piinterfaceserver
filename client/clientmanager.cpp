#include "clientmanager.h"

#include <QDir>
#include <QCoreApplication>
#include <clientsession.h>

#include "remotefolderrequest.h"

ClientManager::ClientManager(QObject *parent) : QObject(parent), m_pendingRequests(0), m_count(0)
{
    m_session = new ClientSession("localhost", 4000, this); //TODO: should have a Connected signal.
    connect(m_session, SIGNAL(connectionStateChanged(QAbstractSocket::SocketState)), SLOT(start()));
}

void ClientManager::start()
{
    /*RaidRequest *req = m_session->getRemoteRaidList();
    connect(req, SIGNAL(raidListChanged(RemoteRaidList*)), SLOT(raidListRetrieved(RemoteRaidList*)));*/
	m_carHandler = m_session->getCarStatus(76000);
	connect(m_carHandler, SIGNAL(ready()), SLOT(carReady()));

    QList<Character> word;
    word << Character("時", "じ", "ji") << Character("間", "かん", "kan");

    m_nihonHandler = m_session->addWord(word, "hour");
    connect(m_nihonHandler, SIGNAL(success()), SLOT(nihonSuccess()));
    connect(m_nihonHandler, SIGNAL(error(int, const QString&)), SLOT(nihonError(int, const QString&)));
    /*RemoteFile *rf;
    int i = 5;
    while (i-- > 0) {
        rf = m_session->getRemoteFile("/home/detlev/myfile.dat");
        connect(rf, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));
    }*/

    //RemoteFolderRequest *f = m_session->getRemoteFolder("/usr/bin/");
    //RemoteFolderRequest *f = m_session->getRemoteFolder("/proc/", "[0-9]*");
    //connect(f, SIGNAL(retrieved(RemoteFolderRequest*)), SLOT(folderRetrieved(RemoteFolderRequest*)));

    //RemoteFile *rf; // FIXME: mamange meory for these
    // Get raid status
    /*m_raidManager = new RaidManager(m_session, this);
    m_raidManager->populate();*/

    //connect(m_raidManager, SIGNAL(raidAdded(RaidEntry*)), SLOT(newRaid(RaidEntry*)));
    //connect(m_raidManager, SIGNAL(raidRemoved(RaidEntry*)), SLOT(removeRaid(RaidEntry*)));
    //connect(m_raidManager, SIGNAL(raidChanged(RaidEntry*)), SLOT(updateRaid(RaidEntry*)));

    m_state = GET_INTERFACES_STATE;

    m_remoteFile = nullptr;
}

void ClientManager::nihonSuccess()
{
    if (dynamic_cast<NihongoAddHandler*>(m_nihonHandler))
    {
        qDebug() << "Word successfully added (id is " << static_cast<NihongoAddHandler*>(m_nihonHandler)->successId() << ")";
        m_nihonHandler->deleteLater();
        m_nihonHandler = m_session->getWords(5);
        connect(m_nihonHandler, SIGNAL(success()), SLOT(nihonSuccess()));
        connect(m_nihonHandler, SIGNAL(error(int, const QString&)), SLOT(nihonError(int, const QString&)));
    }
    else
    {
        NihongoGetHandler *handler = dynamic_cast<NihongoGetHandler*>(m_nihonHandler);
        foreach (NihongoWord *word, handler->words()) {
            qDebug() << word->translation() << ":";
            foreach (Character character, word->characters()) {
                qDebug() << "  " << character.symbol() << "(" << character.kanas() << "|" << character.romajis() << ")";
            }
        }

        handler->deleteLater();
    }
}

void ClientManager::nihonError(int err, const QString &errMessage)
{
    qDebug() << "Error while adding word: " << err << " - " << errMessage;
}

void ClientManager::folderRetrieved(RemoteFolderRequest *folderRequest)
{
    qDebug() << "Folder retrieved with" << folderRequest->remoteFolder()->files().count() << "files";

    /*if (m_count++ == 1)
    RemoteFolderRequest *f = m_session->getRemoteFolder("/usr/bin/");
    connect(f, SIGNAL(retrieved(RemoteFolderRequest*)), SLOT(folderRetrieved(RemoteFolderRequest*)));
*/
/*    foreach (RemoteFile *file, folderRequest->remoteFolder()->files()) {
        qDebug() << file->fileName() << ":" << file->size() << "bytes";
        RemoteFile *rf;
        // Get address
        rf = m_session->getRemoteFile(file->fullFileName() + "/comm");
        connect(rf, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));
        rf = m_session->getRemoteFile(file->fullFileName() + "/stat");
        connect(rf, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));

        m_pendingRequests += 1;
        continue;


        switch(m_state) {
        case GET_INTERFACES_STATE:
        {
            QString fileName;
            RemoteFile *rf;
            m_interfaces << new RemoteNetInterface(file->fileName());
            // Get address
            fileName = file->fullFileName() + "/address";
            rf = m_session->getRemoteFile(fileName);
            connect(rf, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));

            // Get Type
            fileName = file->fullFileName() + "/type";
            rf = m_session->getRemoteFile(fileName);
            connect(rf, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));

            // Get RX
            fileName = file->fullFileName() + "/statistics/rx_bytes";
            rf = m_session->getRemoteFile(fileName);
            connect(rf, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));

            // Get TX
            fileName = file->fullFileName() + "/statistics/tx_bytes";
            rf = m_session->getRemoteFile(fileName);
            connect(rf, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));

            m_pendingRequests += 4;

            m_pendingRequests += 1;
            break;
        }
        }*/

        /*if (file->type() == FolderFileType) {
            RemoteFolderRequest *f = m_session->getRemoteFolder(folderRequest->folder() + QDir::separator() + file->fileName());
            connect(f, SIGNAL(retrieved(RemoteFolderRequest*)), SLOT(folderRetrieved(RemoteFolderRequest*)));
        } else if (file->type() == FileFileType) {
            connect(file, SIGNAL(downloaded(RemoteFile*)), SLOT(fileRetrieved(RemoteFile*)));
            m_session->getRemoteFile(file);
        }*/
   // }*/

    delete folderRequest; //FIXME: won't this delete the RemoteFiles while they are being downloaded ? -->  refcount needed ?

    QCoreApplication::instance()->quit();
}

void ClientManager::fileRetrieved(RemoteFile *remoteFile)
{
    m_pendingRequests -= 1;
    qDebug() << remoteFile->buffer();
    foreach (RemoteNetInterface *iface, m_interfaces) {
        if (!remoteFile->fullFileName().contains("/" + iface->name() + "/"))
            continue;

        bool ok = true;
        if (remoteFile->fullFileName().endsWith("/address"))
            iface->setMac(remoteFile->buffer());
        if (remoteFile->fullFileName().endsWith("/type"))
            iface->setType(remoteFile->buffer());
        if (remoteFile->fullFileName().endsWith("/rx_bytes"))
            iface->setRx(remoteFile->buffer().toULongLong(&ok));
        if (remoteFile->fullFileName().endsWith("/tx_bytes"))
            iface->setTx(remoteFile->buffer().toULongLong(&ok));

        if (!ok) {
            qDebug() << "There was a issue converting" << remoteFile->buffer() << "to a number";
        }

        break;
    }

    if (m_pendingRequests == 0)
        foreach (RemoteNetInterface *iface, m_interfaces) {
            if (iface->type() == "1")
                iface->print();
            else
                qDebug() << iface->name() << iface->type();
        }

    remoteFile->deleteLater();
}

void ClientManager::raidListRetrieved(RemoteRaidList *rrl)
{
    qDebug() << "received LIST";
    foreach (RemoteRaidInfo info, rrl->list()) {
        qDebug() << info.name();
    }

	delete rrl;
}

void ClientManager::carReady()
{
	qDebug() << m_carHandler->kmPc1();
	qDebug() << m_carHandler->kmPc2();
	qDebug() << m_carHandler->timePc();
}

