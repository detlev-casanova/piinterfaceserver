#include <QCoreApplication>
#include "clientmanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ClientManager man(&a);

    return a.exec();
}
