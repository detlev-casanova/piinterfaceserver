#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H

#include <QObject>

#include <clientsession.h>
#include <remotefolder.h>

enum State {
    GET_INTERFACES_STATE,
    GET_MACS_STATE,
    GET_TRX_STATE
};

class RemoteNetInterface {
public:
    RemoteNetInterface(const QString& name) : m_name(name), m_rxBytes(0), m_txBytes(0) {}
    void setRx(uint64_t rx) { m_rxBytes = rx; }
    void setTx(uint64_t tx) { m_txBytes = tx; }
    void setMac(const QString& mac) { m_mac = mac; }
    void setType(const QString& type) { m_type = type; }

    void print() {
        qDebug() << m_name << "\t\t(" << m_mac << "):" << "Up:" << m_txBytes << "|" << "Down:" << m_rxBytes;
    }

    QString name() const { return m_name; }

    QString type() const { return m_type; }

private:
    QString m_name;
    uint64_t m_rxBytes;
    uint64_t m_txBytes;
    QString m_mac;
    QString m_type;
};

class RemoteNetInterface;

class ClientManager : public QObject
{
    Q_OBJECT
public:
    explicit ClientManager(QObject *parent = nullptr);

signals:

private slots:
    void folderRetrieved(RemoteFolderRequest *);
    void fileRetrieved(RemoteFile *);
    void raidListRetrieved(RemoteRaidList*);
	void carReady();
    void start();

    void nihonSuccess();
    void nihonError(int, const QString&);

public slots:
private:
    ClientSession *m_session;
    RemoteFile *m_remoteFile;
    State m_state;
    QList<RemoteNetInterface*> m_interfaces;
    uint8_t m_pendingRequests;

    int m_count;

	CarHandler *m_carHandler;

    NihongoHandler *m_nihonHandler;
};

#endif // CLIENTMANAGER_H
