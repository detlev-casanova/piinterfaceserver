#include "raidpulltask.h"
#include "types.h"

#include "raidtask.h"

RaidPullTask::RaidPullTask(TaskManager *parent)
    : Task(parent)
{
}

uint16_t RaidPullTask::type()
{
    return RAID_MESSAGE_TYPE;
}

bool RaidPullTask::canProcess(Message *m)
{
    return m->type() == type() && (((RaidHeaderBuf*)m->data().data())->type == RAID_REQUEST_INFO || ((RaidHeaderBuf*)m->data().data())->type == RAID_REQUEST_LIST);
}

int RaidPullTask::process(Message *m)
{
    RaidTask *rt = RaidTask::fromData(m->data(), taskManager());
    rt->setSessionId(m->id());
    rt->start();

    return 0;
}
