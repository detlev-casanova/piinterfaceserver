#ifndef CARHANDLER_H
#define CARHANDLER_H

#include <QObject>

class CarHandler : public QObject
{
	Q_OBJECT
public:
	explicit CarHandler(QObject *parent = nullptr);

	unsigned int kmPc1() const;
	void setKmPc1(unsigned int kmPc1);

	unsigned int kmPc2() const;
	void setKmPc2(unsigned int kmPc2);

	unsigned int timePc() const;
	void setTimePc(unsigned int timePc);

signals:
	void ready();

private:
	unsigned int m_kmPc1;
	unsigned int m_kmPc2;
	unsigned int m_timePc;
};

#endif // CARHANDLER_H
