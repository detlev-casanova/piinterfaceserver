#include <QDebug>

#include "message.h"

#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

Message::Message(uint16_t type, const QByteArray &data, uint32_t id)
{
    m_type = type;
    m_data = data;
    if (id == 0)
        m_id = (uint32_t)rand();
    else
        m_id = id;
}

Message *Message::getFromNetworkData(QIODevice *socket)
{
    MessageHeader header;
    Message *message = new Message();
    uint32_t length;
    qint64 read;

    length = sizeof(MessageHeader);
    char *hdr = (char*)&header;
    while (length)
    {
        read = socket->read(hdr, length);
        length -= read;
        hdr += read;

        if (length) {
            //qDebug() << "header cut short";
            socket->waitForReadyRead(100);
        }
    }

    length = ntohl(header.length);
    message->m_id = ntohl(header.id);
    message->m_type = ntohs(header.type);
    message->m_data = QByteArray(length, 0);

    char* data = message->m_data.data();

    while (length) {
        read = socket->read(data, length);

        if (read < 0) {
            qDebug() << "Read returned an error :" << read;
            return nullptr;
        }

        if (read == 0 && length > 0) {
            //qDebug() << "message cut short";
            socket->waitForReadyRead(100);
            //TODO: limit the time this can be done for the same message, fail afterwards (this shouldn't happen on TCP)
        }

        data += read;
        length -= read;
    }

    return message;
}

void Message::setData(const QByteArray &data)
{
    m_data = data;
}

void Message::setId(uint32_t id)
{
    m_id = id;
}

QByteArray Message::toNetworkData()
{
    QByteArray buffer = QByteArray(sizeof(MessageBuffer) + m_data.length(), 0);

    MessageBuffer *message = (MessageBuffer*)buffer.data();
    message->header.type = htons(m_type);
    message->header.length = htonl(m_data.length());
    message->header.id = htonl(m_id);
    memcpy(message->data, m_data.data(), m_data.length());

    return buffer;
}
