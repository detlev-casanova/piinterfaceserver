#ifndef CHARACTER_H
#define CHARACTER_H

#include <QObject>
#include <QJsonObject>

class Character : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString symbol READ symbol WRITE setSymbol NOTIFY symbolChanged)             // The actual Character (can be Kanji, kanas or Katakana)
    Q_PROPERTY(QString kanas READ kanas WRITE setKanas NOTIFY kanasChanged)                 // The pronunciation in kana
    Q_PROPERTY(QString romajis READ romajis WRITE setRomajis NOTIFY romajisChanged)         // The pronunciation in romaji

    QString m_symbol;

    QString m_kanas;

    QString m_romajis;

public:
    explicit Character(QObject *parent = nullptr);
    explicit Character(const QString& symbol, const QString& kanas = QString(), const QString& romajis = QString(), QObject *parent = nullptr);
    Character(const Character& other);

    static QList<Character> listFromJson(const QJsonArray& list);
    static QJsonArray jsonFromList(QList<Character> list);

    static Character fromJson(const QJsonValue& val);
    QJsonObject toJson() const;

    Character operator=(const Character& other);
    bool operator==(const Character& other);

    QString symbol() const
    {
        return m_symbol;
    }
    QString kanas() const
    {
        return m_kanas;
    }

    QString romajis() const
    {
        return m_romajis;
    }

public slots:

    void setSymbol(QString symbol)
    {
        if (m_symbol == symbol)
            return;

        m_symbol = symbol;
        emit symbolChanged(m_symbol);
    }

    void setKanas(QString kanas)
    {
        if (m_kanas == kanas)
            return;

        m_kanas = kanas;
        emit kanasChanged(m_kanas);
    }

    void setRomajis(QString romajis)
    {
        if (m_romajis == romajis)
            return;

        m_romajis = romajis;
        emit romajisChanged(m_romajis);
    }

signals:
    void symbolChanged(QString symbol);
    void kanasChanged(QString kanas);
    void romajisChanged(QString romajis);
};

#endif // CHARACTER_H
