#ifndef RAIDBUFFERS_H
#define RAIDBUFFERS_H


enum RequestType {
    REQUEST_TYPE_ALL = 0,
    REQUEST_TYPE_INFO,
};

struct RaidRequestHeader {
    uint8_t type;
    char data[];
};

struct RaidRequestBuffer {
    RaidRequestHeader hdr;
    uint8_t request;
    char data[];
};

struct Raid

#endif // RAIDBUFFERS_H
