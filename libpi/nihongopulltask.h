#ifndef NIHONGOPULLTASK_H
#define NIHONGOPULLTASK_H

#include <QObject>

#include <task.h>
#include <types.h>

class NihongoPullTask : public Task
{
public:
    NihongoPullTask(TaskManager* parent);

    // Task interface
public:
    uint16_t type() { return NIHON_MESSAGE_TYPE; }
    bool canProcess(Message *m);
    int process(Message *m);
};

#endif // NIHONGOPULLTASK_H
