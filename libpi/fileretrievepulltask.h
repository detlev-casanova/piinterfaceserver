#ifndef FILERETRIEVEPULLTASK_H
#define FILERETRIEVEPULLTASK_H

#include <QObject>

#include "task.h"

class FileRetrievePullTask : public Task
{
public:
    FileRetrievePullTask(TaskManager *parent);

    // Task interface
public:
    uint16_t type();
    bool canProcess(Message *);
    int process(Message *);
};

#endif // FILERETRIEVEPULLTASK_H
