#include "raidinfo.h"

RaidInfo::RaidInfo(QObject *parent) : QObject(parent)
{

}

RaidInfo::RaidInfo(const RaidInfo &other) : QObject(other.parent())
{
    m_name = other.m_name;
}

bool RaidInfo::operator ==(const RaidInfo &other)
{
    return m_name == other.m_name;
}

void RaidInfo::operator =(const RaidInfo &other)
{
    m_name = other.m_name;
}

void RaidInfo::setPersonality(RaidPersonality personality)
{
    if (m_personality == personality)
        return;

    m_personality = personality;
    emit personalityChanged(m_personality);
}

void RaidInfo::setStatus(RaidStatus status)
{
    if (m_status == status)
        return;

    m_status = status;
    emit statusChanged(m_status);
}

void RaidInfo::setDriveCount(int driveCount)
{
    if (m_driveCount == driveCount)
        return;

    m_driveCount = driveCount;
    emit driveCountChanged(m_driveCount);
}

void RaidInfo::setSizeBytes(long sizeBytes)
{
    if (m_sizeBytes == sizeBytes)
        return;

    m_sizeBytes = sizeBytes;
    emit sizeBytesChanged(m_sizeBytes);
}

void RaidInfo::setUsedBytes(long usedBytes)
{
    if (m_usedBytes == usedBytes)
        return;

    m_usedBytes = usedBytes;
    emit usedBytesChanged(m_usedBytes);
}

void RaidInfo::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}

void RaidInfo::setDrives(QStringList drives)
{
    if (m_drives == drives)
        return;

    m_drives = drives;
    emit drivesChanged(m_drives);
}
