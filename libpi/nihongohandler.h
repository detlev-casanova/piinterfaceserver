#ifndef NIHONGOHANDLER_H
#define NIHONGOHANDLER_H

#include <QObject>

#include "message.h"
#include "nihongoword.h"

enum NihongoPicker {
    RandomPicker,B,
};

enum NihongoErrors {
    PARSE_ERROR = 1,
    PROTOCOL_ERROR = 2,
    REPLY_ERROR = 3,
};


class NihongoHandler : public QObject
{
    Q_OBJECT
public:
    explicit NihongoHandler(QObject *parent = nullptr);

    QString reqId() const;
    void setReqId(const QString &reqId);

    static QString reqId(Message *m);
    virtual void parseReply(Message *m) = 0;

signals:
    void success();
    void error(int err, const QString& errorMessage);

private:
    QString m_reqId;
};


class NihongoAddHandler : public NihongoHandler
{
    Q_OBJECT
public:
    NihongoAddHandler(QObject *parent = nullptr);

    Message *getAddWordMessage(NihongoWord* word);
    virtual void parseReply(Message *m);

    quint64 successId() const { return m_successId; }
    //QString errorMessage();

private:
    int m_successId;
};


class NihongoGetHandler : public NihongoHandler
{
    Q_OBJECT
public:
    NihongoGetHandler(QObject *parent = nullptr);

    Message *getGetWordsMessage(quint32 count, const NihongoPicker &picker);
    virtual void parseReply(Message *m);

    static QString pickerToText(const NihongoPicker& picker);
    static NihongoPicker textToPicker(const QString& picker);

    QList<NihongoWord*> words() const { return m_words; }

private:
    QList<NihongoWord*> m_words;
};


class NihongoUpdateHandler : public NihongoHandler
{
    Q_OBJECT
public:
    NihongoUpdateHandler(NihongoWord *word, QObject *parent = nullptr);

    Message *getUpdateWordsMessage(bool success);
    virtual void parseReply(Message *m);

    bool result() const { return m_result; }

    int trend() const { return m_trend; }
    int playCount() const { return m_playCount; }

private:
    bool m_result;
    int m_playCount;
    int m_trend;
    NihongoWord *m_word;
};

#endif // NIHONGOHANDLER_H
