#ifndef TYPES_H
#define TYPES_H

#define HEARTBEAT_MESSAGE_TYPE              0x0001
#define REMOVABLE_DEVICES_MESSAGE_TYPE      0x0002
#define FILESYSTEM_MESSAGE_TYPE             0x0003
#define FILETRANSFER_MESSAGE_TYPE           0x0004
#define RAID_MESSAGE_TYPE                   0x0005
#define CAR_MESSAGE_TYPE					0x0006
#define NIHON_MESSAGE_TYPE                  0x0007

#endif // TYPES_H
