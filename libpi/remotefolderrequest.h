#ifndef REMOTEFOLDERREQUEST_H
#define REMOTEFOLDERREQUEST_H

#include <QObject>

class RemoteFolder;

// TODO: add a filter (e.g. to get /dev/md* files for example)
class RemoteFolderRequest : public QObject {
    Q_OBJECT

    Q_PROPERTY(RemoteFolder* remoteFolder READ remoteFolder WRITE setRemoteFolder)
    Q_PROPERTY(uint32_t id READ id WRITE setId)
    Q_PROPERTY(QString folder READ folder WRITE setFolder)
    Q_PROPERTY(QString matches READ matches WRITE setMatches)

public:
    RemoteFolderRequest(uint32_t id, const QString& folder, const QString& matches = QString(), QObject *parent = 0);
    ~RemoteFolderRequest();

    static RemoteFolderRequest* fromNetworkData(const QByteArray&);

    QByteArray toByteArray() const;

    int parseRemoteFolder(const QByteArray&);

    RemoteFolder* remoteFolder() const
    {
        return m_remoteFolder;
    }

    uint32_t id() const
    {
        return m_id;
    }

    QString folder() const
    {
        return m_folder;
    }

    QString matches() const
    {
        return m_matches;
    }

public slots:
    void setRemoteFolder(RemoteFolder* remoteFolder)
    {
        m_remoteFolder = remoteFolder;
        emit retrieved(this);
    }

    void setId(uint32_t id)
    {
        m_id = id;
    }

    void setFolder(QString folder)
    {
        m_folder = folder;
    }

    void setMatches(QString matches)
    {
        m_matches = matches;
    }

signals:
    void retrieved(RemoteFolderRequest*);

private:
    RemoteFolder* m_remoteFolder;
    QString m_folder;
    uint32_t m_id;
    QString m_matches;

    RemoteFolderRequest() : m_remoteFolder(nullptr), m_id(-1) {}
};

#endif // REMOTEFOLDERREQUEST_H
