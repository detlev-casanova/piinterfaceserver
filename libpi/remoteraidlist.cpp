#include "remoteraidlist.h"
#include "raidtask.h"

struct RaidListBuf {
    RaidHeaderBuf hdr;
    uint8_t count;
    char list[];
} __attribute__((packed));

RemoteRaidList::RemoteRaidList(QObject *parent) : QObject(parent)
{

}

RemoteRaidList *RemoteRaidList::fromNetworkData(const QByteArray &data)
{
    RemoteRaidList *ret = new RemoteRaidList();

    RaidListBuf *buf = (RaidListBuf*)data.data();
    int count = buf->count;
    char *elem = buf->list;
    while (count--) {
        int size = data.size() - sizeof(RaidListBuf);
        RemoteRaidInfo *val = RemoteRaidInfo::fromNetworkData(elem, &size);
        ret->m_list << *val;
        elem += size;
    }

    return ret;
}

QByteArray RemoteRaidList::toNetworkData()
{
    QByteArray ret(sizeof(RaidListBuf), '\0');

    foreach (RemoteRaidInfo val, m_list) {
        ret.append(val.toNetworkData());
    }

    RaidListBuf *buf = (RaidListBuf*) ret.data();
    buf->count = m_list.count();
    buf->hdr.type = RAID_RESPONSE_LIST;

    return ret;
}
