#include "heartbeat.h"

#include <QTimer>
#include <QHostAddress>

#include "types.h"

HeartbeatTask::HeartbeatTask(TaskManager *parent)
    : Task(parent), m_missed(0)
{
    connect(&m_timer, SIGNAL(timeout()), SLOT(probePeer()));
    m_timer.start(10000);
    QTimer::singleShot(0, this, SLOT(probePeer()));
}

void HeartbeatTask::probePeer()
{
    Message *message = new Message(type());
    send(message);

    if (m_missed++ >= 200) {
        qDebug() << "Too many missed heartbeats, disconnecting";
        emit peerNotResponding();
    }
}

uint16_t HeartbeatTask::type()
{
    return HEARTBEAT_MESSAGE_TYPE;
}

bool HeartbeatTask::canProcess(Message *message)
{
    return message->type() == type();
}

int HeartbeatTask::process(Message*)
{
    m_missed = 0;
    return 0;
}
