#include "remotefolder.h"
#include "filesystemtask.h"

#include <arpa/inet.h>

#include <QDir>
#include <QDebug>

struct RemoteFolderBuffer {
    FSRequestHeader hdr;
    uint16_t fileCount;
    RemoteFileBuffer files[];
} __attribute__((packed));

RemoteFolder::~RemoteFolder()
{
    foreach (RemoteFile * f, m_files) {
        delete f;
    }
}

RemoteFolder *RemoteFolder::fromFolderName(const QString &folderName, const QString &matches)
{
    RemoteFolder *folder = new RemoteFolder();
    QDir dir(folderName);
    if (!matches.isEmpty())
    {
        QStringList tmp;
        tmp << matches;
        dir.setNameFilters(tmp);
    }

    foreach (QString f, dir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot)) {
        RemoteFile *newFile = RemoteFile::fromFileName(folderName + QDir::separator() + f);
        folder->m_files << newFile;
    }

    return folder;
}

RemoteFolder *RemoteFolder::fromNetworkData(const QByteArray &data, const QString &folderName)
{
    RemoteFolderBuffer *folder = (RemoteFolderBuffer*)data.data();
    RemoteFolder *f = new RemoteFolder();

    uint16_t fileCount = ntohs(folder->fileCount);
    int processed = sizeof(RemoteFolderBuffer);
    for (int i = 0; i < fileCount; i++) {
        int curr = 0;
        QByteArray files = data.right(data.size() - processed);
        f->addFile(RemoteFile::fromNetworkData(files, &curr, folderName));
        processed += curr;
    }

    return f;
}

QByteArray RemoteFolder::toByteArray() const
{
    QByteArray ret(sizeof(RemoteFolderBuffer), '\0');
    RemoteFolderBuffer *buf = (RemoteFolderBuffer*)ret.data();
    buf->fileCount = htons((uint16_t)m_files.count());
    buf->hdr.request = RESPONSE_FOLDER;

    foreach (RemoteFile *f, m_files) {
        QByteArray arr = f->toByteArray();
        ret += arr;
    }

    return ret;
}

void RemoteFolder::addFile(RemoteFile *file)
{
    m_files << file;
}
