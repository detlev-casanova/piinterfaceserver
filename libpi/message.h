#ifndef MESSAGE_H
#define MESSAGE_H

#include <QByteArray>
#include <QIODevice>

class Message
{
public:
    Message(uint16_t type = 0, const QByteArray& data = QByteArray(), uint32_t id = 0);

    static Message* getFromNetworkData(QIODevice *socket);

    QByteArray data() const { return m_data; }
    uint16_t type() const { return m_type; }
    uint32_t id() const { return m_id; }

    void setData(const QByteArray& data);
    void setType(uint16_t type);
    void setId(uint32_t id);

    QByteArray toNetworkData();

private:
    struct MessageHeader {
        uint16_t type;
        uint32_t id;
        uint32_t length;
    } __attribute__((packed));

    struct MessageBuffer {
        MessageHeader header;
        char data[];
    } __attribute__((packed));

    uint16_t m_type;
    QByteArray m_data;
    uint32_t m_id;
};

#endif // MESSAGE_H
