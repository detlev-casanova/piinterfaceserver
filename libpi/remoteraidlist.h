#ifndef REMOTERAIDLIST_H
#define REMOTERAIDLIST_H

#include <remoteraidinfo.h>

#include <QObject>
#include <QString>

class RemoteRaidList : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<RemoteRaidInfo> list READ list WRITE setList)

    QList<RemoteRaidInfo> m_list;

public:
    explicit RemoteRaidList(QObject *parent = nullptr);

    static RemoteRaidList* fromNetworkData(const QByteArray& data);
    QByteArray toNetworkData();

    QList<RemoteRaidInfo> list() const { return m_list; }

signals:

public slots:
    void setList(QList<RemoteRaidInfo> list) { m_list = list; }
};

#endif // REMOTERAIDLIST_H
