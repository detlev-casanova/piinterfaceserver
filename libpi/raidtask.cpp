#include "raidtask.h"
#include "types.h"
#include "raidmanager.h"

#include <QDebug>
#include <QFile>

RaidTask::RaidTask(TaskManager *parent)
    : Task(parent), m_sessionId(-1), m_raidRequest(nullptr), m_raidList(nullptr)
{

}

uint16_t RaidTask::type()
{
    return RAID_MESSAGE_TYPE;
}

bool RaidTask::canProcess(Message *m)
{
    return m->type() == type() && m->id() == m_sessionId;
}

int RaidTask::process(Message *m)
{
    RaidHeaderBuf *buf = (RaidHeaderBuf *)m->data().data();
    switch (buf->type) {
    case RAID_RESPONSE_LIST:
    {
        RemoteRaidList *list = RemoteRaidList::fromNetworkData(m->data());
        m_raidRequest->setRaidList(list);
        break;
    }
    default:
        qWarning() << "Unknown raid buffer type";
    }

    emit done(this);

    return 0;
}

RaidTask *RaidTask::fromData(const QByteArray &data, TaskManager *manager)
{
    RaidTask * rt = new RaidTask(manager);

    RaidRequest *req = RaidRequest::fromNetworkData(data);
    switch (req->type()) {
    case RAID_REQUEST_LIST:
    {
        rt->m_raidList = new RemoteRaidList();
        break;
    }
    case RAID_RESPONSE_INFO:
    case RAID_RESPONSE_LIST:
    default:
        break;
    }

    return rt;
}

RaidTask *RaidTask::requestList(RaidRequest *req, TaskManager *manager)
{
    RaidTask *t = new RaidTask(manager);
    t->m_raidRequest = req;
    t->m_sessionId = (rand() % 0xffff) + ((rand() % 0xffff) << 16);

    return t;
}

void RaidTask::start()
{
    if (m_raidRequest) {
        Message *m = new Message(type(), m_raidRequest->toNetworkData(), m_sessionId);
        send(m);
    }
    else if (m_raidList) {
        connect(RaidManager::instance(), SIGNAL(raidsChanged(QList<RaidInfo>)), (RaidTask*)this, SLOT(listUpdated(QList<RaidInfo>)));
        RaidManager::instance()->sync();
    }
    else {
        qWarning() << "Nothing to send";
        return;
    }
}

void RaidTask::listUpdated(QList<RaidInfo> list)
{
    QList<RemoteRaidInfo> remoteList;
    foreach (RaidInfo elem, list) {
        remoteList << elem;
    }

    m_raidList->setList(remoteList);

    Message *m = new Message(type(), m_raidList->toNetworkData(), m_sessionId);
    send(m);

    emit done(this);
}
