#include "task.h"
#include <QDebug>

Task::Task(TaskManager *parent)
    : QObject(parent), m_taskManager(parent)
{
    m_taskManager->registerTask(this);
}

Task::~Task()
{

}

void Task::send(Message *message)
{
    m_messageQueue << message;
    emit messageReady(this);
}

