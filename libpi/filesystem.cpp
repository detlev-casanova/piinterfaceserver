#include "filesystem.h"

#include <QDir>
#include <QDebug>


/*
 * TODO: possibility to set it permanent so it can be stored on disk in a DB and reloaded faster.
 */

void printTree(FolderNode * rootNode, int depth = 0)
{
    qDebug() << QString(depth + 1, ' ') << rootNode->foldername << "Folder";
    foreach (FolderNode *f, rootNode->folders) {
        printTree(f, depth + 1);
    }

    foreach (FileNode *f, rootNode->files) {
        qDebug() << QString(depth + 1, ' ') << f->filename << "File";
    }
}

FileSystem::FileSystem(QString filename, FileSystemManager *parent)
    : QObject(parent)
{
    /*m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db.setDatabaseName(":memory:");

    if (!m_db.open())
        return; //m_db.lastError();

    QSqlQuery q;
    // File system as it is (usefull to manipulte files ids)
    if (!q.exec(QLatin1String("create table files(id integer primary key, name varchar, parent_id integer, type integer)")))
        return;//q.lastError();

    // all updates -> deletes and adds (move = delete + add)
    if (!q.exec(QLatin1String("create table updates(id integer primary key, file_id integer, update_type integer)")))
        return;//q.lastError();

    if (!q.prepare(QLatin1String("insert into files(name, parent_id, type) values(?, ?, ?)")))
        return;//q.lastError();
    QVariant root = addFile(q, filename, -1, FileSystemUpdate::FolderFileType);

    qDebug() << m_db.tables();
*/
    m_watcher.addPath(filename);
    m_rootNode = new FolderNode;
    m_rootNode->foldername = filename;

    addSubfolders(m_rootNode);

    qDebug() << m_watcher.directories();

    printTree(m_rootNode);

    /*QSqlQuery query;
    if (query.exec("select name, parent_id, type from files")) {
        query.first();
        do {
            int i = 0;
            do {
                qDebug() << query.value(i++);
            } while (query.value(i).isValid());

        } while (query.next());

    } else
        qDebug() << "Cannot execute query";
*/
    connect(&m_watcher, SIGNAL(directoryChanged(QString)), SLOT(createFolderUpdate(QString)));
    connect(&m_watcher, SIGNAL(fileChanged(QString)), SLOT(createFileUpdate(QString)));
}
/*
QVariant FileSystem::addFile(QSqlQuery &q, const QString &name, int parent_id, int type)
{
    q.addBindValue(name);
    q.addBindValue(parent_id);
    q.addBindValue(type);

    q.exec();

    return q.lastInsertId();
}

QVariant FileSystem::addUpdate(QSqlQuery &q, int file_id, int update_type)
{
    q.addBindValue(file_id);
    q.addBindValue(update_type);

    q.exec();

    return q.lastInsertId();
}*/

// TODO: Maybe add a default max depth and fill lower levels when needed
void FileSystem::addSubfolders(FolderNode *currentNode)
{
    QDir dir(currentNode->foldername);
    /*QSqlQuery q;

    if (!q.prepare(QLatin1String("insert into files(name, parent_id, type) values(?, ?, ?)")))
    {
        qDebug() << q.lastError();
        return;
    }
*/
    qDebug() << currentNode->foldername << ", " << dir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries);

    foreach (QString f, dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs)) {
        qDebug() << "adding " << f;
        m_watcher.addPath(currentNode->foldername + QDir::separator() + f);
        //QVariant new_id = addFile(q, folder + QDir::separator() + f, id, FileSystemUpdate::FolderFileType);
        //qDebug() << new_id;
        FolderNode *newNode = new FolderNode;
        newNode->foldername = currentNode->foldername + QDir::separator() + f;
        addSubfolders(newNode);

        currentNode->folders << newNode;
    }


    foreach (QString f, dir.entryList(QDir::Files)) {
        qDebug() << "adding " << f;
        //m_watcher.addPath(currentNode->foldername + QDir::separator() + f);
        FileNode *newNode = new FileNode;
        newNode->filename = currentNode->foldername + QDir::separator() + f;

        currentNode->files << newNode;
    }
}

void FileSystem::createFolderUpdate(QString folder)
{
    qDebug() << "folder update: " << folder;
}

void FileSystem::createFileUpdate(QString file)
{
    qDebug() << "file update: " << file;
}
