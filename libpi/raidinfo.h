#ifndef RAIDINFO_H
#define RAIDINFO_H

#include <QObject>

enum RaidPersonality {
    LINEAR = 0,
    RAID0 = 1,
    RAID1 = 2,
    RAID4 = 3,
    RAID5 = 4,
    RAID6 = 5,
    RAID10 = 6,
    MULTIPATH = 7,
    FAULTY = 8,
};

enum RaidStatus {
    RAID_ACTIVE = 0,
    RAID_INACTIVE = 1,
};

class RaidInfo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(RaidPersonality personality READ personality WRITE setPersonality NOTIFY personalityChanged)
    Q_PROPERTY(RaidStatus status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(int driveCount READ driveCount WRITE setDriveCount NOTIFY driveCountChanged)
    Q_PROPERTY(long int sizeBytes READ sizeBytes WRITE setSizeBytes NOTIFY sizeBytesChanged)
    Q_PROPERTY(long int usedBytes READ usedBytes WRITE setUsedBytes NOTIFY usedBytesChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QStringList drives READ drives WRITE setDrives NOTIFY drivesChanged)

public:
    explicit RaidInfo(QObject *parent = nullptr);
    RaidInfo(const RaidInfo&);

    RaidPersonality personality() const { return m_personality; }
    RaidStatus status() const { return m_status; }
    int driveCount() const { return m_driveCount; }
    long int sizeBytes() const { return m_sizeBytes; }
    long int usedBytes() const { return m_usedBytes; }
    QString name() const { return m_name; }
    QStringList drives() const { return m_drives; }

    bool operator ==(const RaidInfo& other);
    void operator =(const RaidInfo & other);

signals:
    void personalityChanged(RaidPersonality personality);
    void statusChanged(RaidStatus status);
    void driveCountChanged(int driveCount);
    void sizeBytesChanged(long int sizeBytes);
    void usedBytesChanged(long int usedBytes);
    void nameChanged(QString name);
    void drivesChanged(QStringList drives);

public slots:
    void setPersonality(RaidPersonality personality);
    void setStatus(RaidStatus status);
    void setDriveCount(int driveCount);
    void setSizeBytes(long int sizeBytes);
    void setUsedBytes(long int usedBytes);
    void setName(QString name);
    void setDrives(QStringList drives);

protected:
    RaidPersonality m_personality;

    RaidStatus m_status;

    int m_driveCount;

    long int m_sizeBytes;

    long int m_usedBytes;

    QString m_name;

    QStringList m_drives;
};

#endif // RAIDINFO_H
