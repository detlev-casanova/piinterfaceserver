#ifndef TASK_H
#define TASK_H

#include "message.h"
#include "taskmanager.h"

class Task : public QObject
{
    Q_OBJECT
public:
    Task(TaskManager *parent);
    virtual ~Task();

    virtual uint16_t type() = 0;
    virtual bool canProcess(Message*) = 0;
    virtual int process(Message*) = 0;

    virtual void send(Message*);

    Message* popMessage() { return m_messageQueue.isEmpty() ? nullptr : m_messageQueue.takeFirst(); }

    virtual void messageFailed(Message *message) { Q_UNUSED(message); } //= 0;

signals:
    void messageReady(Task*);
    void done(Task*);

protected:
    TaskManager *taskManager() const { return m_taskManager; }

private:
    QList<Message*> m_messageQueue;
    TaskManager *m_taskManager;
};

#endif // TASK_H
