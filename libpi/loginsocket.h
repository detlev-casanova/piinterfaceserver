#ifndef LOGINSOCKET_H
#define LOGINSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QHash>

class LoginSocket : public QTcpSocket
{
	Q_OBJECT
public:
	enum LoginSocketType {
		LoginSocketClient = 0,
		LoginSocketServer,
	};

	enum LoginSocketState {
		LoginSocketWaitHello = 0,
		LoginSocketWaitLogin = 0,
		LoginSocketOk = 0,
		LoginSocketError = 0,
	};

	Q_PROPERTY(LoginSocketType loginSocketType READ loginSocketType)
    //Q_PROPERTY(QHash credentialsDb READ credentialsDb WRITE setCredentials)
	Q_PROPERTY(QString username READ username WRITE setUsername)
	Q_PROPERTY(QString password READ password WRITE setPassword)

	LoginSocket(LoginSocketType lst, QObject *parent);

	LoginSocketType loginSocketType() const
	{
		return m_loginSocketType;
	}

    QHash<QString,QString> credentialsDb() const
	{
		return m_credentialsDb;
	}

	QString username() const
	{
		return m_username;
	}

	QString password() const
	{
		return m_password;
	}

public slots:
    void setCredentials(QHash<QString,QString> credentials)
	{
		m_credentialsDb = credentials;
	}

	void setUsername(QString username)
	{
		m_username = username;
	}

	void setPassword(QString password)
	{
		m_password = password;
	}

signals:
	void authenticated(LoginSocket*);

private slots:
	void manageStateChange(QAbstractSocket::SocketState);

private:
	LoginSocketType m_loginSocketType;
    QHash<QString,QString> m_credentialsDb;
	QString m_username;
	QString m_password;
};

#endif // LOGINSOCKET_H
