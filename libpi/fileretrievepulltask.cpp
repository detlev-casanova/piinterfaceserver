#include "fileretrievepulltask.h"

#include "types.h"
#include "fileretrievetask.h"

#include <QDebug>

FileRetrievePullTask::FileRetrievePullTask(TaskManager *parent)
 : Task(parent)
{

}

uint16_t FileRetrievePullTask::type()
{
    return FILETRANSFER_MESSAGE_TYPE;
}

bool FileRetrievePullTask::canProcess(Message *m)
{
    return m->type() == type() && FileRetrieveTask::messageType(m->data()) == FT_REQUEST;
}

int FileRetrievePullTask::process(Message *m)
{
    FileRetrieveTask *frt = FileRetrieveTask::fromData(taskManager(), m->data());
    frt->start();

    return 0;
}
