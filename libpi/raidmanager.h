#ifndef RAIDMANAGER_H
#define RAIDMANAGER_H

#include <QObject>
#include <QThread>
#include "raidinfo.h"

class RaidManager : public QThread
{
    Q_OBJECT
    Q_PROPERTY(QList<RaidInfo> raids READ raids WRITE setRaids NOTIFY raidsChanged)
    QList<RaidInfo> m_raids;
    bool m_syncing;

    void run() override;

    RaidManager(QObject *parent = nullptr);

public:
    static RaidManager * instance() {
        static RaidManager * _instance = nullptr;
        if ( _instance == nullptr ) {
            _instance = new RaidManager();
        }
        return _instance;
    }

    void sync();

    QList<RaidInfo> raids() const { return m_raids; }

signals:
    void raidsChanged(QList<RaidInfo> raids);

private slots:
    void setRaids(QList<RaidInfo> raids);
    void workDone();
};

#endif // RAIDMANAGER_H
