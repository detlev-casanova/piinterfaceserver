#include "clientsession.h"
#include "removabledevicestask.h"
#include "fileretrievepulltask.h"
#include "raidpulltask.h"
#include "cartask.h"
#include "carpulltask.h"
#include "nihongopulltask.h"

#include <QHostAddress>
#include <QTextCodec>

// FIXME: should be renamed to PiSession

ClientSession::ClientSession(LoginSocket *socket, QObject *parent)
 : QObject(parent), m_socket(socket), m_mode(ServerMode)
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForUtfText("UT‌​F-8"));

    qRegisterMetaType<QList<RaidInfo> >("QList<RaidInfo>");
    m_taskManager = new TaskManager(this);

    HeartbeatTask *hbt = new HeartbeatTask(m_taskManager);
    //new RemovableDevicesTask(m_taskManager);
    m_fsTask = new FilesystemTask(m_taskManager);
    m_ftTask = new FileRetrievePullTask(m_taskManager);
    new RaidPullTask(m_taskManager);
	new CarPullTask(m_taskManager);
    new NihongoPullTask(m_taskManager);

    connect(m_socket, SIGNAL(readyRead()), this, SLOT(dataReady()));
    connect(m_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SIGNAL(connectionStateChanged(QAbstractSocket::SocketState)));
    connect(hbt, SIGNAL(peerNotResponding()), SLOT(heartbeatLost()));

    qDebug() << "new session created from : " << socket->peerAddress() << ":" << socket->peerPort();
}

ClientSession::ClientSession(const QString& server, quint16 port, QObject * parent)
    : QObject(parent), m_mode(ClientMode), m_server(server), m_port(port)
{
    m_taskManager = new TaskManager(this);

    HeartbeatTask *hbt = new HeartbeatTask(m_taskManager);
    m_fsTask = new FilesystemTask(m_taskManager);

    m_socket = new LoginSocket(LoginSocket::LoginSocketClient, this);
    m_socket->connectToHost(server, port);

    connect(m_socket, SIGNAL(readyRead()), this, SLOT(dataReady()));
    connect(m_socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SIGNAL(connectionStateChanged(QAbstractSocket::SocketState)));
    connect(hbt, SIGNAL(peerNotResponding()), SLOT(heartbeatLost()));
}

RaidRequest *ClientSession::getRemoteRaidInfo(QString name, bool usage, bool detailedUsage)
{
    return nullptr;
}

RaidRequest *ClientSession::getRemoteRaidList()
{
    RaidRequest *req = new RaidRequest(RAID_REQUEST_LIST);
    RaidTask *task = RaidTask::requestList(req, m_taskManager);

    task->start();

	return req;
}

CarHandler *ClientSession::getCarStatus(unsigned int km)
{
	CarTask *task = new CarTask(m_taskManager);
    return task->request(km);
}

NihongoHandler *ClientSession::addWord(const QList<Character>& word, const QString &translation)
{
    NihongoTask *task = new NihongoTask(m_taskManager);
    return task->requestAddWord(word, translation);
}

NihongoHandler *ClientSession::getWords(quint64 count)
{
    NihongoTask *task = new NihongoTask(m_taskManager);
    return task->getWords(count, NihongoPicker::RandomPicker);
}

NihongoHandler *ClientSession::updateWord(NihongoWord *word, bool success)
{
    qDebug() << "ID=" << word->id();
    NihongoTask *task = new NihongoTask(m_taskManager);
    return task->updateWord(word, success);
}

void ClientSession::reconnect()
{
    if (m_socket->state() == QAbstractSocket::UnconnectedState)
        m_socket->connectToHost(m_server, m_port);
}

void ClientSession::dataReady()
{
    while(m_socket->bytesAvailable()) {
        //qDebug() << "Bytes available: " << m_socket->bytesAvailable();
        Message *message = Message::getFromNetworkData(m_socket);
        if (!message)
            continue;

        //qDebug() << "Start process message" << m_socket->bytesAvailable();
        m_taskManager->processMessage(message);
        //qDebug() << "END process message" << m_socket->bytesAvailable();

        delete message;
    }
}

void ClientSession::sendMessage(Task * task)
{
    Message *message;
    while ((message = task->popMessage())) {
        QByteArray toSend = message->toNetworkData();
        if (connectionState() != QAbstractSocket::ConnectedState)
        {
            task->messageFailed(message);
        }

        m_socket->write(toSend);

        delete message;
    }
}

void ClientSession::heartbeatLost()
{
    m_socket->close();
}
