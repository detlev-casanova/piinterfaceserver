QT += core network sql
QT -= gui

CONFIG += c++11

TARGET = pi
CONFIG += lib
CONFIG -= app_bundle

TEMPLATE = lib

SOURCES += clientsession.cpp \
    heartbeat.cpp \
    message.cpp \
    task.cpp \
    taskmanager.cpp \
    removabledevicestask.cpp \
    filesystemtask.cpp \
    filesystemmanager.cpp \
    filesystem.cpp \
    filesystemupdate.cpp \
    remotefolder.cpp \
    remotefolderrequest.cpp \
    remotefile.cpp \
    fileretrievetask.cpp \
    remotefilerequest.cpp \
    fileretrievepulltask.cpp \
    utils.cpp \
    raidtask.cpp \
    raidpulltask.cpp \
    raidrequest.cpp \
    remoteraidlist.cpp \
    remoteraidinfo.cpp \
    raidmanager.cpp \
    raidinfo.cpp \
    carpulltask.cpp \
    cartask.cpp \
    carhandler.cpp \
    loginsocket.cpp \
    nihongotask.cpp \
    nihongopulltask.cpp \
    nihongohandler.cpp \
    nihongoword.cpp \
    character.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    clientsession.h \
    heartbeat.h \
    message.h \
    task.h \
    types.h \
    taskmanager.h \
    removabledevicestask.h \
    filesystemtask.h \
    filesystemmanager.h \
    filesystem.h \
    filesystemupdate.h \
    remotefolder.h \
    remotefolderrequest.h \
    remotefile.h \
    fileretrievetask.h \
    remotefilerequest.h \
    fileretrievepulltask.h \
    utils.h \
    raidtask.h \
    raidpulltask.h \
    raidrequest.h \
    remoteraidlist.h \
    remoteraidinfo.h \
    raidbuffers.h \
    raidmanager.h \
    raidinfo.h \
    carpulltask.h \
    cartask.h \
    carhandler.h \
    loginsocket.h \
    nihongotask.h \
    nihongopulltask.h \
    nihongohandler.h \
    nihongoword.h \
    character.h

INSTALLS        = target

target.files    = libpi.so libpi.so.1
target.path     = /usr/lib
