#include "nihongotask.h"
#include "character.h"
#include "nihongoword.h"
#include "utils.h"
#include "clientsession.h"

#include <QDebug>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include <QDir>

/*
enum NihongoPicker {
    RANDOM_PICKER   = 0, // Will pick random words
    EASY_PICKER     = 1, // Will pick usually correct answers
    HARD_PICKER     = 2, // Will pick usually incorrect answers
    MIXED_PICKER    = 3, // Will pick a mix of the 2, the 6 left bits are used for the threshold:  (picker >> 2) % will be hard and (100 - (picker >> 2)) % will be easy
};*/

class Database {
public:
    Database(const QString & userName) {
        m_db = QSqlDatabase::database("nihongo_" + userName);
        if (!m_db.isValid()) {
                m_db = QSqlDatabase::addDatabase("QSQLITE", "nihongo_" + userName);

                QDir dir("/mnt/Data/db");
                if (!dir.exists())
                    dir.mkpath(".");

                m_db.setDatabaseName("/mnt/Data/db/nihongo_" + userName + ".sqlite");

                if (!m_db.open())
                    qWarning() << "Unable to open database: An error occurred while opening the connection: " << m_db.lastError().text();

                QSqlQuery q("", m_db);
                if (!q.exec("create table IF NOT EXISTS Words (id integer primary key, Translation varchar, PlayCount number, Trend number)"))
                    qDebug() << "cannot create table Words";
                if (!q.exec("create table IF NOT EXISTS Characters (id integer primary key, WordId integer, Position integer, Symbol varchar, Kanas varchar, Romajis varchar)"))
                    qDebug() << "cannot create table Kanas";

                m_db.close();
        }

        m_db.open();
    }

    ~Database() {
        m_db.close();
    }

    qint64 addWord(const NihongoWord* word) {
        QSqlQuery q("", m_db);
        int wordId;
        int count = 0;
        QString str = QString("insert into Words (Translation, PlayCount, Trend) values ('%1', 0, 0)").arg(word->translation());
        qDebug() << str;
        q.exec(str);

        wordId = q.lastInsertId().toInt();

        foreach (Character character, word->characters()) {
            str = QString("insert into Characters (WordId, Position, Symbol, Kanas, Romajis) values (%1, %2, '%3', '%4', '%5')").arg(wordId).arg(count++).arg(character.symbol()).arg(character.kanas()).arg(character.romajis());
            qDebug() << str;
            q.exec(str);
        }

        return wordId;
    }

    bool delWord(quint64 id);
    bool editWord(quint64 id, QHash<QString, QVariant>);
    NihongoWord getWord(quint64 id);

    QList<NihongoWord*> getWords(int count) {
        QList<NihongoWord*> ret;
        QSqlQuery q("", m_db);
        QString str = QString("select * from Words order by random() %1").arg(count == 0 ? "" : (QString(" limit %1").arg(count)));
        qDebug() << str;
        q.exec(str);

        while (q.next()) {
            NihongoWord *word = new NihongoWord();
            int id = q.value("id").toInt();
            word->setId((quint64)id);
            word->setCharacters(getCharacterList(id));//TODO: use a joint
            word->setTranslation(q.value("translation").toString());

            ret << word;
        }

        return ret;
    }

    bool updateWord(int id, bool success, int *playCount, int *trend) {
        QSqlQuery q("", m_db);
        QString str = QString("update Words set Trend = MAX(0, MIN(Trend %1 1, 10)) , PlayCount = PlayCount + 1 where id = %2").arg(success ? "+" : "-").arg(id);
        qDebug() << str;
        if (!q.exec(str)) {
            qDebug() << q.lastError().text();
            return false;
        }

        q.clear();

        if (!q.exec(QString("SELECT PlayCount, Trend FROM Words WHERE id=%1").arg(id)))
        {
            qDebug() << q.executedQuery();
            qDebug() << q.lastError().text();
            return false;
        }

        qDebug() << q.executedQuery();

        while(q.next())
        {
            *playCount = q.value("PlayCount").toInt();
            *trend = q.value("Trend").toInt();
        }

        qDebug() << "PC=" << *playCount << "T=" << *trend;

        return true;
    }

private:
    QSqlDatabase m_db;

    QList<Character> getCharacterList(int id) {
        QList<Character> ret;
        QSqlQuery q("", m_db);
        QString str = QString("select * from Characters where WordId = %1 order by Position ASC").arg(id);
        qDebug() << str;
        q.exec(str);

        while (q.next()) {
            Character character;
            character.setSymbol(q.value("Symbol").toString());
            character.setKanas(q.value("Kanas").toString());
            character.setRomajis(q.value("Romajis").toString());

            ret << character;
        }

        return ret;
    }
};

NihongoTask::NihongoTask(TaskManager *parent)
 : Task(parent)
{

}

void NihongoTask::reply()
{
    QJsonObject obj;
    obj["type"] = "reply";
    obj["req_id"] = m_request.object()["req_id"];

    QString request = m_request.object()["type"].toString();

    if (request == "add_word")
    {
        Database db(taskManager()->session()->userName());
        qint64 newId = db.addWord(NihongoWord::fromJson(m_request.object()["word"].toObject()));

        obj["status"] = "okay";
        obj["id"] = QJsonValue(newId);
    }
    else if (request == "get_words")
    {
        QJsonArray wordArray;
        Database db(taskManager()->session()->userName());
        QList<NihongoWord*> words = db.getWords(m_request.object()["count"].toInt(0));
        foreach (NihongoWord *word, words) {
            wordArray << word->toJson();
        }

        obj["words"] = wordArray;
        obj["status"] = "okay";
    }
    else if (request == "update_word")
    {
        int playCount, trend;
        Database db(taskManager()->session()->userName());
        bool success = m_request.object()["success"] == "yes";
        db.updateWord(m_request.object()["id"].toInt(), success, &playCount, &trend);

        obj["playcount"] = playCount;
        obj["trend"] = trend;
        obj["status"] = "okay";
    }
    else
    {
        qDebug() << "Action" << request << "is not supported";
        obj["status"] = "error";
        obj["errorstr"] = "Action " + request + " is not supported";
    }

    Message *m = new Message(NIHON_MESSAGE_TYPE, QJsonDocument(obj).toJson());
    send(m);

    done(this);
}

NihongoTask *NihongoTask::fromByteArray(const QByteArray &data, TaskManager *parent)
{
    NihongoTask *ret = new NihongoTask(parent);
    if (ret)
    {
        ret->m_request = QJsonDocument::fromJson(data);
    }
    return ret;
}

bool NihongoTask::canProcess(Message *m)
{
    return m->type() == NIHON_MESSAGE_TYPE && isReply(m) && NihongoHandler::reqId(m) == m_handler->reqId();
}

int NihongoTask::process(Message *m)
{
    m_handler->parseReply(m);

    done(this);

    return 0;
}

bool NihongoTask::isReply(Message *m)
{
    QJsonParseError error;
    QJsonDocument jDoc = QJsonDocument::fromJson(m->data(), &error);
    if (jDoc.isNull())
    {
        qWarning() << "Cannot parse document JSON:" << error.errorString();
        return false;
    }

    if (!jDoc.isObject())
    {
        qWarning() << "JSON is not valid";
        return false;
    }

    QJsonObject obj = jDoc.object();

    if (!obj.contains("type"))
    {
        qWarning() << "Json document doesn't have a type value";
        return false;
    }

    QJsonValue val = obj["type"];

    return val.isString() && val.toString() == "reply";
}

NihongoAddHandler *NihongoTask::requestAddWord(const QList<Character> &word, const QString &translation)
{
    QJsonObject core;

    NihongoWord nihongoWord;
    nihongoWord.setCharacters(word);
    nihongoWord.setTranslation(translation);

    NihongoAddHandler *handler = new NihongoAddHandler(this);

    send(handler->getAddWordMessage(&nihongoWord));

    m_handler = handler;

    return handler;
}

NihongoGetHandler *NihongoTask::getWords(unsigned int count, const NihongoPicker &picker)
{
    NihongoGetHandler *handler = new NihongoGetHandler(this);

    send(handler->getGetWordsMessage(count, picker));

    m_handler = handler;

    return handler;
}

NihongoUpdateHandler *NihongoTask::updateWord(NihongoWord *word, bool success)
{
    NihongoUpdateHandler *handler = new NihongoUpdateHandler(word, this);
    send(handler->getUpdateWordsMessage(success));
    m_handler = handler;

    return handler;
}
