#include "carpulltask.h"

#include "cartask.h"

CarPullTask::CarPullTask(TaskManager *parent)
    : Task(parent)
{

}

bool CarPullTask::canProcess(Message *m)
{
	return m->type() == CAR_MESSAGE_TYPE && CarTask::isRequest(m);
}

int CarPullTask::process(Message *m)
{
	CarTask *task = CarTask::fromNetworkData(m->data(), taskManager());

	if (task == NULL)
		return -1;

	task->reply();

	return 0;
}
