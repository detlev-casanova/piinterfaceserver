#ifndef HEARTBEAT_H
#define HEARTBEAT_H

#include <QTcpSocket>
#include <QTimer>

#include "task.h"

class HeartbeatTask : public Task
{
    Q_OBJECT
public:
    explicit HeartbeatTask(TaskManager *parent);

signals:
    void peerNotResponding();

public slots:
    void probePeer();

private:
    QTimer m_timer;
    int m_missed;

    // Task interface
public:
    uint16_t type();
    bool canProcess(Message *);
    int process(Message *);
};

#endif // HEARTBEAT_H
