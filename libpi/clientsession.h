#ifndef CLIENTSESSION_H
#define CLIENTSESSION_H

#include <QTcpSocket>

#include "task.h"
#include "taskmanager.h"
#include "heartbeat.h"
#include "filesystemtask.h"
#include "raidtask.h"
#include "carhandler.h"
#include "loginsocket.h"
#include "nihongotask.h"

class FileRetrievePullTask;

class ClientSession : public QObject /*FIXME: still could be a QIODevice*/
{
    Q_OBJECT
    Q_PROPERTY(QAbstractSocket::SocketState connectionState READ connectionState WRITE setConnectionState NOTIFY connectionStateChanged)
public:
    /**
     * @brief ClientSession Create a server side ClientSession
     * @param socket
     * @param parent
     */
    ClientSession(LoginSocket *socket, QObject * parent = nullptr);
    ClientSession(const QString& server, quint16 port, QObject * parent = nullptr);

    RemoteFolderRequest *getRemoteFolder(const QString& folder, const QString& matches = QString()) { return m_fsTask->getRemoteFolder(folder, matches); }
    void getRemoteFile(RemoteFile * remoteFile, const QString& destFile = QString()) { m_fsTask->retrieveRemoteFile(remoteFile, destFile); }
    RemoteFile* getRemoteFile(const QString& fileName, const QString& destFile = QString()) { return m_fsTask->retrieveRemoteFile(fileName, destFile); }
    RaidRequest *getRemoteRaidInfo(QString name, bool usage, bool detailedUsage);
    RaidRequest *getRemoteRaidList();
    CarHandler *getCarStatus(unsigned int km);
    NihongoHandler *addWord(const QList<Character> &word, const QString& translation);
    NihongoHandler *getWords(quint64 count);
    NihongoHandler *updateWord(NihongoWord *word, bool success);

    QAbstractSocket::SocketState connectionState() const
    {
        return m_socket->state();
    }

    QString userName() const { qDebug() << "Warning: userName not implemented"; return "detlev"; }

public slots:
    void setConnectionState(QAbstractSocket::SocketState connectionState)
    {
        Q_UNUSED(connectionState);
    }

    void reconnect();

signals:
    void quit();
    void connectionStateChanged(QAbstractSocket::SocketState connectionState);

private slots:
    void dataReady();
    void sendMessage(Task*);
    void heartbeatLost();

private:
    LoginSocket * m_socket;
    TaskManager *m_taskManager;
    enum ConnectionMode {
        ServerMode,
        ClientMode
    };

    ConnectionMode m_mode;

    FilesystemTask *m_fsTask;
    FileRetrievePullTask *m_ftTask;

    QString m_server;
    quint16 m_port;
};

#endif // CLIENTSESSION_H
