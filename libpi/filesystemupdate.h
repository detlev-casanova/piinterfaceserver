#ifndef FILESYSTEMUPDATE_H
#define FILESYSTEMUPDATE_H

#include <QObject>

class FileSystemUpdate : public QObject {
    Q_OBJECT

public:
    FileSystemUpdate(QObject *parent = 0);

    enum FileType {
        LinkFileType,
        FileFileType,
        FolderFileType,
    };

    enum ChangeType {
        AddedChangeType,
        RemovedChangeType,
        ModifiedChangeType,
        PermissionsChangeType,
    };

    Q_PROPERTY(QString filename READ filename WRITE setFilename)
    Q_PROPERTY(FileType fileType READ fileType WRITE setFileType)
    Q_PROPERTY(ChangeType changeType READ changeType WRITE setChangeType)

    FileType fileType() const
    {
        return m_fileType;
    }

    QString filename() const
    {
        return m_filename;
    }

    ChangeType changeType() const
    {
        return m_changeType;
    }

public slots:
    void setFileType(FileType fileType)
    {
        m_fileType = fileType;
    }
    void setFilename(QString filename)
    {
        m_filename = filename;
    }
    void setChangeType(ChangeType changeType)
    {
        m_changeType = changeType;
    }

private:
    FileType m_fileType;
    QString m_filename;
    ChangeType m_changeType;
};

#endif // FILESYSTEMUPDATE_H
