#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QObject>
#include <QList>

class Task;
class ClientSession;
class Message;

class TaskManager : public QObject
{
    Q_OBJECT
public:
    explicit TaskManager(ClientSession *parent);

    void registerTask(Task*);
    bool processMessage(Message *message);

    ClientSession *session() const { return m_clientSession; }

private slots:
    void taskDone(Task*);

private:
    QList<Task*> m_tasks;
    ClientSession *m_clientSession;

};

#endif // TASKMANAGER_H
