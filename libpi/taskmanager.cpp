#include "taskmanager.h"

#include "clientsession.h"
#include "message.h"
#include "task.h"

TaskManager::TaskManager(ClientSession *parent)
    : QObject(parent)
{
    m_clientSession = parent;
}

void TaskManager::registerTask(Task *task)
{
    connect(task, SIGNAL(messageReady(Task*)), m_clientSession, SLOT(sendMessage(Task*)));
    connect(task, SIGNAL(done(Task*)), SLOT(taskDone(Task*)));
    m_tasks << task;
}

bool TaskManager::processMessage(Message *message)
{
    foreach (Task *task, m_tasks) {
        if (task->canProcess(message)) {
            task->process(message);
            return true;
        }
    }

    qWarning() << "Nobody wanted to process a message of type " << message->type();

    return false;
}

void TaskManager::taskDone(Task *task)
{
    m_tasks.removeAll(task);
    delete task;
}
