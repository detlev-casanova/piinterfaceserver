#ifndef NIHONGOTASK_H
#define NIHONGOTASK_H

#include <QObject>
#include <QJsonDocument>

#include "types.h"
#include "task.h"

#include "nihongohandler.h"
#include "character.h"

class NihongoTask : public Task
{
public:
    NihongoTask(TaskManager* parent);

    void reply();

    static NihongoTask* fromByteArray(const QByteArray& data, TaskManager* parent);

    // Task interface
public:
    uint16_t type() { return NIHON_MESSAGE_TYPE; }
    bool canProcess(Message *);
    int process(Message *m);

    static bool isReply(Message *m);

    NihongoAddHandler * requestAddWord(const QList<Character> &word, const QString& translation);
    NihongoGetHandler *getWords(unsigned int count, const NihongoPicker& picker);
    NihongoUpdateHandler *updateWord(NihongoWord *word, bool success);
    NihongoHandler * removeWord(quint64 id);

private:
    NihongoHandler* m_handler;
    QJsonDocument m_request;

};

#endif // NIHONGOTASK_H
