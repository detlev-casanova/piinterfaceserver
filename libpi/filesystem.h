#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <QObject>
#include <QFileSystemWatcher>

#include "filesystemmanager.h"
#include "filesystemupdate.h"

struct FileNode {
    QString filename;
};

struct FolderNode
{
    QString foldername;
    QList<FileNode*> files;
    QList<FolderNode*> folders;
};

class FileSystem : public QObject
{
    Q_OBJECT
public:
    explicit FileSystem(QString folder, FileSystemManager *parent = 0);
    QList<FileSystemUpdate> getUpdatesSince(int lastId);

signals:
    void updated(const FileSystemUpdate& update);

public slots:

private slots:
    void createFolderUpdate(QString);
    void createFileUpdate(QString);

private:
    QFileSystemWatcher m_watcher;
    void addSubfolders(FolderNode *currentNode);
    FolderNode *m_rootNode;

    //QSqlDatabase m_db;
    //QVariant addFile(QSqlQuery &q, const QString &name, int parent_id, int type);
    //QVariant addUpdate(QSqlQuery &q, int file_id, int update_type);
};

#endif // FILESYSTEM_H
