#ifndef RAIDREQUEST_H
#define RAIDREQUEST_H

#include <QObject>

#include "remoteraidinfo.h"
#include "remoteraidlist.h"

#include "message.h"

enum RaidBufType {
    RAID_REQUEST_LIST,
    RAID_REQUEST_INFO,
    RAID_RESPONSE_LIST,
    RAID_RESPONSE_INFO,
    RAID_NONE_TYPE,
};

class RaidRequest : public QObject
{
    Q_OBJECT
    Q_PROPERTY(RaidBufType type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(bool usage READ usage WRITE setUsage NOTIFY usageChanged)
    Q_PROPERTY(bool detailedUsage READ detailedUsage WRITE setDetailedUsage NOTIFY detailedUsageChanged)
    Q_PROPERTY(RemoteRaidInfo* raidInfo READ raidInfo WRITE setRaidInfo NOTIFY raidInfoChanged)
    Q_PROPERTY(RemoteRaidList* raidList READ raidList WRITE setRaidList NOTIFY raidListChanged)

    RaidBufType m_type;

    bool m_usage;

    bool m_detailedUsage;

    RemoteRaidInfo* m_raidInfo;

    RemoteRaidList* m_raidList;

public:
    explicit RaidRequest(RaidBufType type, QObject *parent = nullptr);

    RaidBufType type() const
    {
        return m_type;
    }

    bool usage() const
    {
        return m_usage;
    }

    bool detailedUsage() const
    {
        return m_detailedUsage;
    }

    QByteArray toNetworkData() const;
    static RaidRequest* fromNetworkData(const QByteArray& data);
    static bool isRequestMessage(Message *message);

    RemoteRaidInfo* raidInfo() const
    {
        return m_raidInfo;
    }

    RemoteRaidList* raidList() const
    {
        return m_raidList;
    }

signals:

    void typeChanged(RaidBufType type);

    void usageChanged(bool usage);

    void detailedUsageChanged(bool detailedUsage);

    void raidInfoChanged(RemoteRaidInfo* raidInfo);

    void raidListChanged(RemoteRaidList* raidList);

public slots:
    void setType(RaidBufType type)
    {
        if (m_type == type)
            return;

        m_type = type;
        emit typeChanged(m_type);
    }
    void setUsage(bool usage)
    {
        if (m_usage == usage)
            return;

        m_usage = usage;
        emit usageChanged(m_usage);
    }
    void setDetailedUsage(bool detailedUsage)
    {
        if (m_detailedUsage == detailedUsage)
            return;

        m_detailedUsage = detailedUsage;
        emit detailedUsageChanged(m_detailedUsage);
    }
    void setRaidInfo(RemoteRaidInfo* raidInfo)
    {
        if (m_raidInfo == raidInfo)
            return;

        m_raidInfo = raidInfo;
        emit raidInfoChanged(m_raidInfo);
    }
    void setRaidList(RemoteRaidList* raidList)
    {
        if (m_raidList == raidList)
            return;

        m_raidList = raidList;
        emit raidListChanged(m_raidList);
    }
};

#endif // RAIDREQUEST_H
