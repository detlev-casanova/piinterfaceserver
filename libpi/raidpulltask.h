#ifndef RAIDPULLTASK_H
#define RAIDPULLTASK_H

#include <QObject>

#include "task.h"

class RaidPullTask : public Task
{
public:
    RaidPullTask(TaskManager *parent);
    
    // Task interface
public:
    uint16_t type();
    bool canProcess(Message *);
    int process(Message *);
};

#endif // RAIDPULLTASK_H
