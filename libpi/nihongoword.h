#ifndef NIHONGOWORD_H
#define NIHONGOWORD_H

#include <QString>
#include <QList>

#include <QJsonArray>

#include "character.h"

/*
 * Word definition in json:
 * {
 *      trend: <int>
 *      playcount: <int>
 *      translation: <string>
 *      id: <int>
 *      characters: LIST<Character>
 * }
 *
 * Character definition in json:
 *
 * {
 *      symbol: <string>
 *      kanas: <string>
 *      romajis: <string>
 * }
 */

class NihongoWord : public QObject
{
    Q_OBJECT
public:
    NihongoWord(QObject *parent = nullptr);
    NihongoWord(const NihongoWord& other);

    static NihongoWord* fromJson(const QJsonObject &);
    QJsonObject toJson() const;

    NihongoWord operator=(const NihongoWord& other);
    bool operator==(const NihongoWord& other);
    QString translation() const;
    void setTranslation(const QString &translation);
    QList<Character> characters() const;
    void setCharacters(const QList<Character> &characters);
    qint16 trend() const;
    void setTrend(const qint16 &trend);
    quint64 playCount() const;
    void setPlayCount(const quint64 &playCount);

    quint64 id() const;
    void setId(const quint64 &id);

signals:
    void trendChanged(int trend);

private:
    QList<Character> m_word;
    QString m_translation;
    qint16 m_trend;
    quint64 m_playCount;
    quint64 m_id;

    /*Dont forget to copy new members ;) */

};

#endif // NIHONGOWORD_H
