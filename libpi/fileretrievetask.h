#ifndef FILERETRIEVETASK_H
#define FILERETRIEVETASK_H

#include "task.h"

#include <QFile>

class RemoteFile;

enum FTMessageType {
    FT_REQUEST  = 0,
    FT_RESPONSE = 1,
    FT_CHUNK    = 2,
};

enum FTDirection {
    DirectionRequester,
    DirectionResponder
};

class FileRetrieveTask : public Task
{
public:
    FileRetrieveTask(TaskManager *parent, RemoteFile *remoteFile = NULL, const QString& destFile=QString());

    static FileRetrieveTask* fromData(TaskManager *parent, const QByteArray&data);

    void start();

    static FTMessageType messageType(const QByteArray&);

    // Task interface
public:
    uint16_t type();
    bool canProcess(Message *m);
    int process(Message *m);

private:
    uint64_t m_sessionId;
    RemoteFile *m_remoteFile;
    QString m_localFileName;
    QString m_destFileName;
    QFile m_localFile;
    FTDirection m_direction;
    void sendNextChunk();
};

#endif // FILERETRIEVETASK_H
