#ifndef REMOTEFILE_H
#define REMOTEFILE_H

#include <QObject>
#include <QByteArray>
#include <QDir>

#include <QDebug>

#include <stdint.h>

struct RemoteFileBuffer {
    uint64_t size;
    uint32_t permissions;
    uint8_t type;
    uint16_t fileNameSize;
    char fileName[];
} __attribute__((packed));

enum FileType {
    LinkFileType    = 0,
    FileFileType    = 1,
    FolderFileType  = 2,
};

class RemoteFile : public QObject {
    Q_OBJECT
public:
    RemoteFile(QObject *parent = 0);
    static RemoteFile *fromNetworkData(const QByteArray& data, int * processed = NULL, const QString& folderName=QString());
    static RemoteFile *fromFileName(const QString& fileName);
    QByteArray toByteArray();

    /**
     * @brief retrieve the remote file in the given @file.
     * downloaded() is emitted when the file has been fully retrived.
     * if @file is empty, the data will be stored in memory.
     */
    void retrieve(const QString &file=QString());

    /**
     * @brief watch Start watch the remote file for events
     * @return 0 on success, -1 if it failed (TODO: define errors)
     */
    int watch(/* TODO: Define FLAGS to watch only a subset of event types */);

    Q_PROPERTY(uint64_t size READ size WRITE setSize NOTIFY sizeChanged)
    Q_PROPERTY(uint32_t permissions READ permissions WRITE setPermissions NOTIFY permissionsChanged)
    Q_PROPERTY(uint8_t type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName NOTIFY fileNameChanged)
    Q_PROPERTY(QString folderName READ folderName WRITE setFolderName)

    uint64_t size() const
    {
        return m_size;
    }

    uint32_t permissions() const
    {
        return m_permissions;
    }

    uint8_t type() const
    {
        return m_type;
    }

    QString fileName() const
    {
        return m_fileName;
    }

    QString fullFileName() const
    {
        return m_folderName + QDir::separator() + m_fileName;
    }

    QByteArray buffer() const
    {
        return m_buffer;
    }

    QString folderName() const;
    void setFolderName(const QString &folderName);

public slots:
    void setSize(uint64_t size)
    {
        if (m_size == size)
            return;

        m_size = size;
        emit sizeChanged(size);
    }

    void setPermissions(uint32_t permissions)
    {
        if (m_permissions == permissions)
            return;

        m_permissions = permissions;
        emit permissionsChanged(permissions);
    }

    void setType(uint8_t type)
    {
        if (m_type == type)
            return;

        m_type = type;
        emit typeChanged(type);
    }

    void setFileName(QString fileName)
    {
        if (m_fileName == fileName)
            return;

        m_fileName = fileName;
        emit fileNameChanged(fileName);
    }

    void appendData(char * data, uint16_t len)
    {
        m_buffer.append(data, len);
    }

signals:
    void downloaded(RemoteFile*);

    void sizeChanged(uint32_t size);

    void permissionsChanged(uint32_t permissions);

    void typeChanged(uint8_t type);

    void fileNameChanged(QString fileName);

    void failure(RemoteFile*);

private:

    uint64_t m_size;
    uint32_t m_permissions;
    uint8_t m_type;
    QString m_fileName;
    QString m_folderName;
    QByteArray m_buffer;
};

#endif // REMOTEFILE_H
