#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>
#include <QString>

class Utils
{
public:
    Utils();
    static uint64_t htonll(uint64_t value);
    static uint64_t ntohll(uint64_t value);
    static QString randomString(int len);
};

#endif // UTILS_H
