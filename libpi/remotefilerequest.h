#ifndef REMOTEFILEREQUEST_H
#define REMOTEFILEREQUEST_H

#include <QObject>

class RemoteFile;

/**
 * @brief The RemoteFileRequest class is used to retrieve the content of a file.
 */
class RemoteFileRequest : public QObject {
    Q_OBJECT
    Q_PROPERTY(RemoteFile* remoteFile READ remoteFile WRITE setRemoteFile)
    Q_PROPERTY(uint32_t id READ id WRITE setId)
    Q_PROPERTY(QString fileName READ fileName WRITE setFile)

public:
    RemoteFileRequest(uint32_t id = 0, const QString& fileName = QString(), QObject *parent = 0);

    static RemoteFileRequest* fromNetworkData(const QByteArray&);

    QByteArray toByteArray() const;

    int parseRemoteFile(const QByteArray&);

    RemoteFile* remoteFile() const
    {
        return m_remoteFile;
    }/* --> TODO: should be data();*/

    uint32_t id() const
    {
        return m_id;
    }

    QString fileName() const
    {
        return m_file;
    }

public slots:
    void setRemoteFile(RemoteFile* remoteFile)
    {
        m_remoteFile = remoteFile;
        emit retrieved(this);
    }

    void setId(uint32_t id)
    {
        m_id = id;
    }

    void setFile(const QString& file)
    {
        m_file = file;
    }

signals:
    void retrieved(RemoteFileRequest*);

private:
    RemoteFile* m_remoteFile;
    QString m_file;
    uint32_t m_id;
};

#endif // REMOTEFILEREQUEST_H
