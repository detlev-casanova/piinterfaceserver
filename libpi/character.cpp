#include "character.h"
#include <QJsonArray>

Character::Character(QObject *parent) : QObject(parent)
{

}

Character::Character(const QString &symbol, const QString &kanas, const QString &romajis, QObject *parent)
    : QObject(parent), m_symbol(symbol), m_kanas(kanas), m_romajis(romajis)
{

}

Character::Character(const Character &other)
    : QObject(other.parent())
{
    m_symbol = other.symbol();
    m_kanas = other.kanas();
    m_romajis = other.romajis();
}

QJsonArray Character::jsonFromList(QList<Character> list)
{
    QJsonArray ret;

    foreach (Character character, list) {
        ret << character.toJson();
    }

    return ret;
}

Character Character::fromJson(const QJsonValue &val)
{
    Character ret;

    ret.m_symbol = val.toObject()["symbol"].toString();
    ret.m_kanas = val.toObject()["kanas"].toString();
    ret.m_romajis = val.toObject()["romajis"].toString();

    return ret;
}

QJsonObject Character::toJson() const
{
    QJsonObject ret;

    ret["symbol"] = symbol();
    ret["kanas"] = kanas();
    ret["romajis"] = romajis();

    return ret;
}

Character Character::operator=(const Character &other)
{
    m_symbol = other.symbol();
    m_kanas = other.kanas();
    m_romajis = other.romajis();

    return *this;
}

bool Character::operator==(const Character &other)
{
    return m_symbol == other.symbol() &&
            m_kanas == other.kanas() &&
            m_romajis == other.romajis();
}
