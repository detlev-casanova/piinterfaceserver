#ifndef REMOVABLEDEVICESTASK_H
#define REMOVABLEDEVICESTASK_H

#include "task.h"
//#include <QDBusConnection>

/**
 * @brief The RemovableDevicesTask class notifies clients about new removable devices.
 * Used to mount them, umount them, format them, list them.
 */

class RemovableDevicesTask : public Task
{
    Q_OBJECT
public:
    RemovableDevicesTask(TaskManager *parent);

private slots:
    void dbusInterfaceAdded();
    // Task interface
public:
    uint16_t type();
    bool canProcess(Message *);
    int process(Message *);

private:
};

#endif // REMOVABLEDEVICESTASK_H
