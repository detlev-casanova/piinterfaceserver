#include "nihongohandler.h"
#include "types.h"
#include "utils.h"

#include <QJsonDocument>
#include <QDebug>

NihongoHandler::NihongoHandler(QObject *parent) : QObject(parent)
{
    m_reqId = Utils::randomString(20);
}

QString NihongoHandler::reqId() const
{
    return m_reqId;
}

void NihongoHandler::setReqId(const QString &reqId)
{
    m_reqId = reqId;
}

QString NihongoHandler::reqId(Message *m)
{
    QJsonDocument doc = QJsonDocument::fromJson(m->data());

    return doc.object()["req_id"].toString("");
}

NihongoAddHandler::NihongoAddHandler(QObject *parent)
    : NihongoHandler(parent)
{}

Message *NihongoAddHandler::getAddWordMessage(NihongoWord *word)
{
    QJsonObject core;
    core["type"] = "add_word";
    core["req_id"] = reqId();
    core["word"] = word->toJson();

    QJsonDocument doc(core);
    return new Message(NIHON_MESSAGE_TYPE, doc.toJson());
}

void NihongoAddHandler::parseReply(Message *m)
{
    QString errorMessage;

    QJsonParseError jError;
    QJsonDocument jDoc = QJsonDocument::fromJson(m->data(), &jError);

    if (jDoc.isNull())
    {
        errorMessage = "Cannot parse document JSON:" + jError.errorString();
        emit error(PARSE_ERROR, errorMessage);
        return;
    }

    if (!jDoc.isObject())
    {
        errorMessage = "JSON is not valid";
        emit error(PROTOCOL_ERROR, errorMessage);
        return;
    }

    if (jDoc.object()["status"] != "okay")
    {
        errorMessage = jDoc.object()["error_str"].toString("Unknown server error");
        emit error(REPLY_ERROR, errorMessage);
        return;
    }

    m_successId = jDoc.object()["id"].toInt();
    emit success();
}


NihongoGetHandler::NihongoGetHandler(QObject *parent)
    : NihongoHandler(parent)
{

}

Message *NihongoGetHandler::getGetWordsMessage(quint32 count, const NihongoPicker &picker)
{
    QJsonObject core;

    core["type"] = "get_words";
    core["req_id"] = reqId();
    core["count"] = QJsonValue(static_cast<qint64>(count));
    core["picker"] = pickerToText(picker);

    QJsonDocument doc(core);

    return new Message(NIHON_MESSAGE_TYPE, doc.toJson());
}

void NihongoGetHandler::parseReply(Message *m)
{
    QString errorMessage;

    QJsonParseError jError;
    QJsonDocument jDoc = QJsonDocument::fromJson(m->data(), &jError);

    if (jDoc.isNull())
    {
        errorMessage = "Cannot parse document JSON:" + jError.errorString();
        emit error(PARSE_ERROR, errorMessage);
        return;
    }

    if (!jDoc.isObject())
    {
        errorMessage = "JSON is not valid";
        emit error(PROTOCOL_ERROR, errorMessage);
        return;
    }

    if (jDoc.object()["status"] != "okay")
    {
        errorMessage = jDoc.object()["error_str"].toString("Unknown server error");
        emit error(REPLY_ERROR, errorMessage);
        return;
    }

    foreach (QJsonValue jWord, jDoc.object()["words"].toArray()) {
        m_words << NihongoWord::fromJson(jWord.toObject());
    }

    emit success();
}

QString NihongoGetHandler::pickerToText(const NihongoPicker &picker)
{
    switch (picker) {
    case RandomPicker:
        return "random";
    case B:
        return "B";
    }

    return "random";
}

NihongoPicker NihongoGetHandler::textToPicker(const QString &picker)
{
    if (picker == "random")
        return RandomPicker;

    return RandomPicker;
}

NihongoUpdateHandler::NihongoUpdateHandler(NihongoWord *word, QObject *parent)
    : NihongoHandler(parent), m_word(word)
{

}

Message *NihongoUpdateHandler::getUpdateWordsMessage(bool success)
{
    QJsonObject core;
    core["type"] = "update_word";
    core["req_id"] = reqId();
    core["id"] = (int)m_word->id();
    core["success"] = success ? "yes" : "no";

    QJsonDocument doc(core);
    return new Message(NIHON_MESSAGE_TYPE, doc.toJson());
}

void NihongoUpdateHandler::parseReply(Message *m)
{
    QString errorMessage;

    QJsonParseError jError;
    QJsonDocument jDoc = QJsonDocument::fromJson(m->data(), &jError);

    if (jDoc.isNull())
    {
        errorMessage = "Cannot parse document JSON:" + jError.errorString();
        emit error(PARSE_ERROR, errorMessage);
        return; // Should emit failure()
    }

    if (!jDoc.isObject())
    {
        errorMessage = "JSON is not valid";
        emit error(PROTOCOL_ERROR, errorMessage);
        return;
    }

    if (jDoc.object()["status"] != "okay")
    {
        errorMessage = jDoc.object()["error_str"].toString("Unknown server error");
        emit error(REPLY_ERROR, errorMessage);
        return;
    }

    m_word->setTrend(jDoc.object()["trend"].toInt());
    m_word->setPlayCount(jDoc.object()["playcount"].toInt());

    emit success();
}
