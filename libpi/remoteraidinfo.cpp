#include "remoteraidinfo.h"
#include "raidtask.h"

#include <arpa/inet.h>

struct RaidInfoBuf {
    uint8_t personality;
    uint8_t driveCount;
    uint8_t status;
    uint32_t sizeBytes;
    uint32_t usedBytes;
    uint16_t listLen;
    char list[]; // Contains raid name (/dev/md0), devices name (/dev/sda1, /dev/sdb1,..)
} __attribute__((packed));

RemoteRaidInfo::RemoteRaidInfo(QObject *parent) : RaidInfo(parent)
{

}

RemoteRaidInfo::RemoteRaidInfo(const RaidInfo &other)
{
    m_name = other.name();
}

RemoteRaidInfo *RemoteRaidInfo::fromNetworkData(const QByteArray &data)
{
    int size = data.size();
    return RemoteRaidInfo::fromNetworkData(data.data(), &size);
}

RemoteRaidInfo *RemoteRaidInfo::fromNetworkData(const char *data, int *size)
{
    RemoteRaidInfo *info;
    RaidInfoBuf *buf = (RaidInfoBuf*)data;
    int maxLen = *size;

    if (maxLen < sizeof(RaidInfoBuf) || maxLen < sizeof(RaidInfoBuf) + ntohs(buf->listLen))
        return nullptr;

    info = new RemoteRaidInfo();

    info->m_personality = (RaidPersonality)buf->personality;
    info->m_driveCount = buf->driveCount;
    info->m_status = (RaidStatus)buf->status;
    info->m_sizeBytes = ntohl(buf->sizeBytes);
    info->m_usedBytes = ntohl(buf->usedBytes);

    char *next = buf->list;

    info->m_name = QString(next);
    next += info->m_name.length() + 1;

    char *end = ((char*)buf) + ntohs(buf->listLen);

    while (next < end) {
        QString drive = QString(next);
        info->m_drives << drive;

        next += drive.length() + 1;
    }

    *size = sizeof(RaidInfoBuf) + ntohs(buf->listLen);

    return info;
}

QByteArray RemoteRaidInfo::toNetworkData()
{
    QByteArray ret(sizeof(RaidInfoBuf), '\0');
    RaidInfoBuf *buf = (RaidInfoBuf*)ret.data();

    buf->driveCount = m_driveCount;
    buf->status = m_status;
    buf->personality = m_personality;
    buf->sizeBytes = htonl(m_sizeBytes);
    buf->usedBytes = htonl(m_usedBytes);

    ret += m_name.toUtf8();
    ret += '\0';

    foreach (QString drive, m_drives) {
        ret += drive.toUtf8();
        ret += '\0';
    }

    buf->listLen = ret.size() - sizeof(RaidInfoBuf);

    return ret;
}

