#ifndef CARPULLTASK_H
#define CARPULLTASK_H

#include <QObject>

#include "task.h"
#include "types.h"

class CarPullTask : public Task
{
public:
	CarPullTask(TaskManager *parent);

	// Task interface
public:
	uint16_t type() { return CAR_MESSAGE_TYPE; }
	bool canProcess(Message *m);
	int process(Message *m);
};

#endif // CARPULLTASK_H
