#include "removabledevicestask.h"

#include "types.h"

//#include <QDBusConnection>
#include <QDebug>

RemovableDevicesTask::RemovableDevicesTask(TaskManager *parent)
    : Task(parent)
{
    /*
     * DBUS signal to register to:
     * signal time=1492273092.630945 sender=:1.11 -> destination=(null destination) serial=1649 path=/org/freedesktop/UDisks2; interface=org.freedesktop.DBus.ObjectManager; member=InterfacesAdded
       object path "/org/freedesktop/UDisks2/drives/Verbatim_STORE_N_GO_13092600001101"
     */

    /*if (QDBusConnection::systemBus().connect("", "/org/freedesktop/UDisks2", "org.freedesktop.DBus.ObjectManager", "DeviceAdded", this, SLOT(dbusInterfaceAdded())))
        qDebug() << "Registered to dbus udisks2 notifications";
    else
        qDebug() << "Cannot register to dbus";*/
}

void RemovableDevicesTask::dbusInterfaceAdded()
{
    qDebug() << "DEVICE ADDED";
}

uint16_t RemovableDevicesTask::type()
{
    return REMOVABLE_DEVICES_MESSAGE_TYPE;
}

bool RemovableDevicesTask::canProcess(Message *m)
{
    return m->type() == type();
}

int RemovableDevicesTask::process(Message *m)
{
    qDebug() << "RemovableDevicesTask";
    return 0;
}
