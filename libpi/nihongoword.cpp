#include "nihongoword.h"

#include <QJsonObject>
#include <QJsonValue>

qint16 NihongoWord::trend() const
{
    return m_trend;
}

void NihongoWord::setTrend(const qint16 &trend)
{
    if (m_trend != trend)
    {
        m_trend = trend;
        emit trendChanged((int)trend);
    }
}

quint64 NihongoWord::playCount() const
{
    return m_playCount;
}

void NihongoWord::setPlayCount(const quint64 &playCount)
{
    m_playCount = playCount;
}

quint64 NihongoWord::id() const
{
    return m_id;
}

void NihongoWord::setId(const quint64 &id)
{
    m_id = id;
}

NihongoWord::NihongoWord(QObject *parent)
    : QObject(parent), m_trend(0), m_playCount(0)
{

}

NihongoWord::NihongoWord(const NihongoWord &other)
    : QObject(other.parent())
{
    setCharacters(other.m_word);
    setTranslation(other.m_translation);
    setPlayCount(other.m_playCount);
    setTrend(other.m_trend);
    setId(other.m_id);
}

NihongoWord* NihongoWord::fromJson(const QJsonObject & json)
{
    NihongoWord *ret = new NihongoWord();

    ret->m_trend = static_cast<quint64>(json["trend"].toInt(0));
    ret->m_playCount = static_cast<quint64>(json["playcount"].toInt(0));
    ret->m_translation = json["translation"].toString("????");
    ret->m_id = static_cast<quint64>(json["id"].toInt(0));

    foreach (QJsonValue character, json["characters"].toArray()) {
        ret->m_word << Character::fromJson(character);
    }

    return ret;
}

QJsonObject NihongoWord::toJson() const
{
    QJsonObject ret;

    ret["id"] = QJsonValue((qint64)id());
    ret["trend"] = QJsonValue((qint64)trend());
    ret["playcount"] = QJsonValue((qint64)playCount());
    ret["translation"] = translation();

    QJsonArray charArray;
    foreach (Character character, m_word) {
        charArray << character.toJson();
    }

    ret["characters"] = charArray;

    return ret;
}

NihongoWord NihongoWord::operator=(const NihongoWord &other)
{
    setCharacters(other.m_word);
    setTranslation(other.m_translation);
    setPlayCount(other.m_playCount);
    setTrend(other.m_trend);
    setId(other.m_id);

    return *this;
}

bool NihongoWord::operator==(const NihongoWord &other)
{
    return id() == other.id() &&
            characters() == other.characters() &&
            translation() == other.translation() &&
            playCount() == other.playCount() &&
            trend() == other.trend();
}


QList<Character> NihongoWord::characters() const
{
    return m_word;
}

void NihongoWord::setCharacters(const QList<Character> &word)
{
    m_word = word;
}

QString NihongoWord::translation() const
{
    return m_translation;
}

void NihongoWord::setTranslation(const QString &translation)
{
    m_translation = translation;
}
