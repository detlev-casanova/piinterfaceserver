#include "raidrequest.h"
#include "raidtask.h"

struct RaidRequestBuf {
    RaidHeaderBuf hdr;
    uint8_t requestUsage: 1;
    uint8_t requestDetailedUsage: 1;
    char data[];
} __attribute__((packed));

RaidRequest::RaidRequest(RaidBufType type, QObject *parent) : QObject(parent), m_type(type), m_usage(false), m_detailedUsage(false)
{
    
}

QByteArray RaidRequest::toNetworkData() const
{
    QByteArray req(sizeof(RaidRequestBuf), '\0');

    RaidRequestBuf *reqBuf = (RaidRequestBuf*)req.data();
    reqBuf->hdr.type = m_type;
    reqBuf->requestDetailedUsage = detailedUsage();
    reqBuf->requestUsage = usage();

    return req;
}

RaidRequest *RaidRequest::fromNetworkData(const QByteArray &data)
{
    RaidRequestBuf *buf = (RaidRequestBuf*)data.data();
    RaidRequest *req = new RaidRequest((RaidBufType)buf->hdr.type);

    return req;
}
