#include "nihongopulltask.h"
#include "nihongotask.h"
#include "types.h"

#include <QDebug>

NihongoPullTask::NihongoPullTask(TaskManager *parent)
 : Task(parent)
{

}

bool NihongoPullTask::canProcess(Message *m)
{
    return m->type() == NIHON_MESSAGE_TYPE && !NihongoTask::isReply(m);
}

int NihongoPullTask::process(Message *m)
{
    NihongoTask *task = NihongoTask::fromByteArray(m->data(), taskManager());

    if (task == nullptr)
        return -1;

    task->reply();

    return 0;
}
