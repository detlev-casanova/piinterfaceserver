#ifndef FILESYSTEMTASK_H
#define FILESYSTEMTASK_H

#include "task.h"
#include "remotefile.h"

#include <stdint.h>
#include <QFile>

class RemoteFolderRequest;
class RemoteFileRequest;

struct FSRequestHeader {
    uint8_t request;
    //char data[];
} __attribute__((packed));

enum RequestType {
    REQUEST_FOLDER = 0,
    RESPONSE_FOLDER = 1,
    REQUEST_SEND_FILE,
    REQUEST_RECV_FILE,
    RESPONSE_FILE_CHUNK,
    REQUEST_DEL_FILE,
    REQUEST_MOVE_FILE,
    REQUEST_COPY_FILE,
};

/**
 * @brief The FilesystemTask class manages FS operations :
 *  * List files
 *  * copy/move files
 *  * remove files
 *  * ??
 */
class FilesystemTask : public Task
{
public:
    FilesystemTask(TaskManager *parent);

private:
    void processFolderRequest(Message *);
    void processFolderResponse(Message *);
    void processFileRequest(Message *);

    QList<RemoteFolderRequest*> m_folderRequests;
    QList<RemoteFileRequest*> m_fileRequests;

    // Task interface
public:
    uint16_t type();
    bool canProcess(Message *);
    int process(Message *);


    /* Get a remote folder information and files information */
    RemoteFolderRequest * getRemoteFolder(const QString &folder, const QString &matches = QString());

    /* Get a remote file information */
    RemoteFileRequest * getRemoteFile(const QString &file);

    //FIXME: should be moved to ClientSession
    /* Get a file content */
    void retrieveRemoteFile(RemoteFile *remoteFile, const QString& destFile=QString());
    /* Get a file content
     * Provided for convenience: a destination file mus be provided as no RemoteFile buffer can be used
     */
    RemoteFile *retrieveRemoteFile(const QString& remoteFileName, const QString& destFile);
};

#endif // FILESYSTEMTASK_H
