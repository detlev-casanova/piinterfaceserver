#ifndef REMOTERAIDINFO_H
#define REMOTERAIDINFO_H

#include <QObject>
#include "raidinfo.h"

class RemoteRaidInfo : public RaidInfo
{
    Q_OBJECT

public:
    explicit RemoteRaidInfo(QObject *parent = nullptr);
    RemoteRaidInfo(const RaidInfo&);
    static RemoteRaidInfo *fromNetworkData(const char *data, int *size);
    static RemoteRaidInfo *fromNetworkData(const QByteArray& data);
    QByteArray toNetworkData();
};

#endif // REMOTERAIDINFO_H
