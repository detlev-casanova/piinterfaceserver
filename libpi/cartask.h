#ifndef CARTASK_H
#define CARTASK_H

#include <QObject>
#include <QDate>

#include "task.h"
#include "types.h"

class CarHandler;

class CarTask : public Task
{
public:
	CarTask(TaskManager *parent);

	// Task interface
public:
	uint16_t type() { return CAR_MESSAGE_TYPE; }
	bool canProcess(Message *m);
	int process(Message *m);

	static bool isRequest(Message *m);
	static bool isResponse(Message *m);

	static CarTask* fromNetworkData(const QByteArray& data, TaskManager *);

	CarHandler* request(unsigned int km);
	void reply();

private:
	QDateTime m_startDate;
	QDateTime m_today;
	unsigned int m_kmPerYear;
	unsigned int m_leaseYearCount;
	unsigned int m_currentKm;

	CarHandler *m_handler;
};

#endif // CARTASK_H
