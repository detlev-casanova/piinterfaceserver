#include "raidmanager.h"

#include <QFile>
#include <QDebug>

void RaidManager::run()
{
    // TODO: try to update existing ones as much as possible.
    QList<RaidInfo> ret;
    QFile file("/proc/mdstat");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "No mdstat, is it compiled in the kernel ?";
        setRaids(ret); // TODO: emit an error instead
        return;
    }

    QList<QByteArray> lines = file.readAll().split('\n');
    foreach (QString line, lines) {
        if (!line.startsWith("Personalities : ") && !line.startsWith("unused devices: ") && !line.startsWith(" ") && !line.isEmpty()) {
            RaidInfo info;
            info.setName(line.split(':')[0].trimmed());
            ret << info;
        }
    }

    file.close();

    setRaids(ret);
}

RaidManager::RaidManager(QObject *parent) : QThread(parent), m_syncing(false)
{
    connect(this, SIGNAL(finished()), SLOT(workDone()));
}

void RaidManager::sync()
{
    if (m_syncing)
        return;

    m_syncing = true;

    start();
}

void RaidManager::setRaids(QList<RaidInfo> raids)
{
    m_raids = raids;
    emit raidsChanged(m_raids);
}

void RaidManager::workDone()
{
    m_syncing = false;
}
