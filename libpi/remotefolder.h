#ifndef REMOTEFOLDER_H
#define REMOTEFOLDER_H

#include <QObject>

class RemoteFile;

class RemoteFolder : public QObject {
    Q_OBJECT
public:
    ~RemoteFolder();
    static RemoteFolder *fromFolderName(const QString& folderName, const QString& matches);
    static RemoteFolder *fromNetworkData(const QByteArray& data, const QString& folderName);
    QByteArray toByteArray() const;

    void addFile(RemoteFile* file);

    QList<RemoteFile*> files() const { return m_files; }
private:
    QList<RemoteFile*> m_files;
};

#endif // REMOTEFOLDER_H
