#include "remotefolderrequest.h"
#include "remotefolder.h"
#include "filesystemtask.h"
#include <QDebug>

// TODO: unify Folder and File requests ?

struct FolderRequestHeader {
    FSRequestHeader hdr;
    char folderName[];
} __attribute__((packed));

RemoteFolderRequest::RemoteFolderRequest(uint32_t id, const QString &folder, const QString &matches, QObject *parent)
    : QObject(parent), m_remoteFolder(nullptr), m_folder(folder), m_id(id), m_matches(matches)
{
}

RemoteFolderRequest::~RemoteFolderRequest()
{
    if (m_remoteFolder != nullptr)
        delete m_remoteFolder;
}

RemoteFolderRequest *RemoteFolderRequest::fromNetworkData(const QByteArray& data)
{
    RemoteFolderRequest *req = new RemoteFolderRequest();
    FolderRequestHeader *hdr = (FolderRequestHeader*)data.data();
    req->m_folder = hdr->folderName;
    req->m_matches = hdr->folderName + strlen(hdr->folderName) + 1;

    return req;
}

QByteArray RemoteFolderRequest::toByteArray() const
{
    QByteArray ret(sizeof(FolderRequestHeader), '\0');
    FolderRequestHeader *buf = (FolderRequestHeader*)ret.data();
    buf->hdr.request = REQUEST_FOLDER;
    ret.append(m_folder);
    ret.append(1, '\0');
    ret.append(m_matches);
    ret.append(1, '\0');

    return ret;
}

