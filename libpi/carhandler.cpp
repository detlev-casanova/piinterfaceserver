#include "carhandler.h"

CarHandler::CarHandler(QObject *parent) : QObject(parent)
{

}

unsigned int CarHandler::kmPc1() const
{
	return m_kmPc1;
}

void CarHandler::setKmPc1(unsigned int kmPc1)
{
	m_kmPc1 = kmPc1;
}

unsigned int CarHandler::kmPc2() const
{
	return m_kmPc2;
}

void CarHandler::setKmPc2(unsigned int kmPc2)
{
	m_kmPc2 = kmPc2;
}

unsigned int CarHandler::timePc() const
{
	return m_timePc;
}

void CarHandler::setTimePc(unsigned int timePc)
{
	m_timePc = timePc;
}
