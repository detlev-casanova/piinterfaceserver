#include "filesystemtask.h"
#include "remotefile.h"
#include "remotefolder.h"
#include "remotefolderrequest.h"
#include "remotefilerequest.h"

#include "fileretrievetask.h"

#include "types.h"

#include <QDebug>
#include <QDir>

#include <libgen.h>

FilesystemTask::FilesystemTask(TaskManager *parent)
    : Task(parent)
{
    //FileSystemManager *m = new FileSystemManager(this);
    //new FileSystem("/mnt/Data", m);
}

RemoteFolderRequest *FilesystemTask::getRemoteFolder(const QString& folder, const QString& matches)
{
    Message *m = new Message(type());
    RemoteFolderRequest *ret = new RemoteFolderRequest(m->id(), folder, matches);

    m_folderRequests << ret;
    m->setData(ret->toByteArray());
    send(m);

    return ret;
}

/*
 * TODO: Add the possibility to save directly into a local file
 */

void FilesystemTask::retrieveRemoteFile(RemoteFile *remoteFile, const QString &destFile)
{
    FileRetrieveTask *newTask = new FileRetrieveTask(taskManager(), remoteFile, destFile);
    newTask->start();
    //m_fileRetrieveTasks << newTask;
}

RemoteFile* FilesystemTask::retrieveRemoteFile(const QString &remoteFileName, const QString &destFile)
{
    RemoteFile * remoteFile = new RemoteFile();
    remoteFile->setFileName(basename(remoteFileName.toUtf8().data()));
    remoteFile->setFolderName(dirname(remoteFileName.toUtf8().data()));

    FileRetrieveTask *newTask = new FileRetrieveTask(taskManager(), remoteFile, destFile);

    newTask->start();

    return remoteFile;
}

void FilesystemTask::processFolderRequest(Message *m)
{
    // TODO: do this in RemoteFolderRequest
    // TODO: don't do this in the main thread
    RemoteFolderRequest *req = RemoteFolderRequest::fromNetworkData(m->data());
    qDebug() << "Getting " << req->matches() << "from " << req->folder();

    RemoteFolder *rfl = RemoteFolder::fromFolderName(req->folder(), req->matches());

    Message *message = new Message(type(), rfl->toByteArray(), m->id());
    delete rfl;
    delete req;
    send(message);
}

void FilesystemTask::processFolderResponse(Message *m)
{
    foreach (RemoteFolderRequest *req, m_folderRequests) {
        if (req->id() == m->id()) {
            RemoteFolder *rFolder = RemoteFolder::fromNetworkData(m->data(), req->folder());
            req->setRemoteFolder(rFolder);
            m_folderRequests.removeAll(req);
            break;
        }
    }
}

void FilesystemTask::processFileRequest(Message *m)
{
    FileRetrieveTask *frt = FileRetrieveTask::fromData(taskManager(), m->data());
}

uint16_t FilesystemTask::type()
{
    return FILESYSTEM_MESSAGE_TYPE;
}

bool FilesystemTask::canProcess(Message *m)
{
    return m->type() == type();
}

int FilesystemTask::process(Message *m)
{
    /*
     * Listing files should be done in a separate thread and send a message when it is ready
     */

    uint8_t request = m->data()[0];
    switch(request) {
    case REQUEST_FOLDER:
        processFolderRequest(m);
        break;
    case RESPONSE_FOLDER:
        processFolderResponse(m);
        break;
    case REQUEST_DEL_FILE:
    case REQUEST_MOVE_FILE:
    case REQUEST_COPY_FILE:
    default:
        qDebug() << "Unknown File System request: " << request;
        return -1;
    }

    return 0;
}
