#include "fileretrievetask.h"
#include "remotefile.h"
#include "types.h"

#include <utils.h>
#include <arpa/inet.h>

#include <QDebug>

enum FileStatus {
    FILE_READ_OK = 0,
    FILE_READ_END,
    FILE_READ_ERROR,
};

// TODO: fix hton and ntoh !

// TODO: create handler classes (could be subclasses of Message)
struct FileTransferHeader {
    uint64_t sessionId;
    uint8_t type;
    //char data[];
} __attribute__((packed));

// TODO: could add a 'length' field to retrive only length first bytes
// TODO: could add a 'start' and an 'end' fields to retrive parts of the file
struct FileTransferRequest {
    FileTransferHeader hdr;
    char requestFileName[];
} __attribute__((packed));

struct FileTransferResponse {
    FileTransferHeader hdr;
    uint8_t status; //TODO: use an enum for statuses
    uint32_t fileSize;
} __attribute__((packed));

struct FileTransferChunk {
    FileTransferHeader hdr;
    uint32_t number;
    uint16_t length;
    char data[];
} __attribute__((packed));

FileRetrieveTask::FileRetrieveTask(TaskManager *parent, RemoteFile *remoteFile, const QString &destFile)
 : Task(parent)
{
    m_sessionId = (rand() % 0xFFFF) + ((rand() % 0xFFFF) << 16) + (((uint64_t)rand() % 0xFFFF) << 32) + (((uint64_t)rand() % 0xFFFF) << 48);
    m_remoteFile = remoteFile;
    m_direction = DirectionRequester;
    m_destFileName = destFile;
}

FileRetrieveTask *FileRetrieveTask::fromData(TaskManager *parent, const QByteArray & data)
{
    FileTransferHeader* hdr = (FileTransferHeader*)data.data();
    if (hdr->type != FT_REQUEST)
        return NULL;

    FileRetrieveTask *ret = new FileRetrieveTask(parent);

    ret->m_direction = DirectionResponder;
    ret->m_sessionId = Utils::ntohll(hdr->sessionId);
    ret->m_localFileName = ((FileTransferRequest*)data.data())->requestFileName;

    //qDebug() << data.toHex();

    return ret;
}

void FileRetrieveTask::start()
{
    switch (m_direction)
    {
    case DirectionRequester:
    {
        // Send FileTransferRequest
        FileTransferRequest *req = (FileTransferRequest *)new char[sizeof(FileTransferRequest) + m_remoteFile->fullFileName().toUtf8().size() + 1];

        //TODO: check filename encoding --> Maybe better to always process them in UTF-8
        strcpy(req->requestFileName, m_remoteFile->fullFileName().toUtf8().data());
        req->hdr.sessionId = Utils::htonll(m_sessionId);
        req->hdr.type = FT_REQUEST;

        QByteArray array((char*)req, sizeof(FileTransferRequest) + m_remoteFile->fullFileName().toUtf8().size() + 1);

        //qDebug() << array.toHex();

        delete [] req;

        Message *m = new Message(FILETRANSFER_MESSAGE_TYPE, array);
        send(m);
        break;
    }
    case DirectionResponder:
    {
        FileTransferResponse *res = new FileTransferResponse;

        m_localFile.setFileName(m_localFileName);
        if (!m_localFile.open(QIODevice::ReadOnly))
        {
            qDebug() << "Cannot open" << m_localFileName;
            res->status = FILE_READ_ERROR;
        }
        else
            res->status = FILE_READ_OK;

        res->hdr.sessionId = Utils::htonll(m_sessionId);
        res->fileSize = htonl((uint32_t)m_localFile.size());
        res->hdr.type = FT_RESPONSE;

        QByteArray array((char*)res, sizeof(FileTransferResponse));

        uint8_t status = res->status;

        delete [] res;

        Message *m = new Message(FILETRANSFER_MESSAGE_TYPE, array);
        send(m);

        if (status == FILE_READ_OK)
            sendNextChunk();
        else
            emit done(this);
        break;
    }
    }
}

FTMessageType FileRetrieveTask::messageType(const QByteArray& data)
{
    return (FTMessageType)((FileTransferHeader*)data.data())->type;
}

uint16_t FileRetrieveTask::type()
{
    return FILETRANSFER_MESSAGE_TYPE;
}

bool FileRetrieveTask::canProcess(Message *m)
{
    return m->type() == FILETRANSFER_MESSAGE_TYPE && Utils::ntohll(((FileTransferHeader *)m->data().data())->sessionId) == m_sessionId;
}

int FileRetrieveTask::process(Message *m)
{
    if (((FileTransferHeader *)m->data().data())->type == FT_CHUNK)
    {
        QByteArray data = m->data();
        FileTransferChunk * chunk = (FileTransferChunk *)data.data();
        uint16_t len = ntohs(chunk->length);
        m_remoteFile->appendData(chunk->data, len);
    }
    else if (((FileTransferHeader *)m->data().data())->type == FT_RESPONSE)
    {
        if (((FileTransferResponse *)m->data().data())->status == FILE_READ_END)
            m_remoteFile->downloaded(m_remoteFile);
        else if (((FileTransferResponse *)m->data().data())->status == FILE_READ_ERROR)
            m_remoteFile->failure(m_remoteFile);
    }

    return 0;
}

void FileRetrieveTask::sendNextChunk()
{
    //TODO: read by chunks
    QByteArray data_chunk = m_localFile.readAll();
    if (!data_chunk.isEmpty()) {
        FileTransferChunk *ftc = (FileTransferChunk*) new char[sizeof(FileTransferChunk) + data_chunk.size()];

        ftc->hdr.sessionId = Utils::htonll(m_sessionId);
        ftc->hdr.type = FT_CHUNK;
        ftc->length = htons((uint16_t)data_chunk.size());
        ftc->number = 0;
        memcpy(ftc->data, data_chunk.data(), data_chunk.size());

        QByteArray array((char*)ftc, sizeof(FileTransferChunk) + data_chunk.size());

        delete [] ftc;

        Message *m = new Message(FILETRANSFER_MESSAGE_TYPE, array);
        send(m);
    }
    m_localFile.close();

    // Send a DONE message
    //if (lastChunkSent)
    FileTransferResponse *res = new FileTransferResponse;

    res->status = data_chunk.isEmpty() ? FILE_READ_ERROR : FILE_READ_END;
    res->hdr.sessionId = Utils::htonll(m_sessionId);
    res->fileSize = 0;
    res->hdr.type = FT_RESPONSE;

    QByteArray array2((char*)res, sizeof(FileTransferResponse));

    delete res;

    Message *m = new Message(FILETRANSFER_MESSAGE_TYPE, array2);
    send(m);

    emit done(this);

    // FIXME: Would also need an error message if the file was deleted while reading or modified or shrunk
}
