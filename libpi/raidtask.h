#ifndef RAIDTASK_H
#define RAIDTASK_H

#include <QObject>

#include "task.h"
#include "raidrequest.h"

struct RaidHeaderBuf {
    uint8_t type;
    //char data[];
} __attribute__((packed));

class RaidTask : public Task
{
    Q_OBJECT
public:
    explicit RaidTask(TaskManager *parent);
    
    // Task interface
    Q_PROPERTY(uint32_t sessionId READ sessionId WRITE setSessionId)
public:
    uint16_t type();
    bool canProcess(Message *);
    int process(Message *);

    static RaidTask *fromData(const QByteArray& data, TaskManager *manager);
    static RaidTask *requestList(RaidRequest *req, TaskManager *manager);

    void start();
    
    uint32_t sessionId() const
    {
        return m_sessionId;
    }

public slots:
    void setSessionId(uint32_t sessionId)
    {
        m_sessionId = sessionId;
    }

private slots:
    void listUpdated(QList<RaidInfo>);

private:
    uint32_t m_sessionId;

    RaidBufType m_request;
    RaidBufType m_response;

    RaidRequest *m_raidRequest;
    RemoteRaidList *m_raidList;

    QStringList getRaidList();
};

#endif // RAIDTASK_H
