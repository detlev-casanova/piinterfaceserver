#include "loginsocket.h"

LoginSocket::LoginSocket(LoginSocketType lst, QObject *parent = nullptr) : QTcpSocket(parent)
{
	connect(this, SIGNAL(stateChanged(QAbstractSocket::SocketState)), SLOT(manageStateChange(QAbstractSocket::SocketState)));
}

void LoginSocket::manageStateChange(QAbstractSocket::SocketState state)
{
    if (state == QAbstractSocket::ConnectedState)
    {
        emit authenticated(this);
    }
}
