#include "remotefilerequest.h"

#include "filesystemtask.h"
#include <QDebug>

// TODO: unify File and File requests

struct FileRequestHeader {
    FSRequestHeader hdr;
    char fileName[];
} __attribute__((packed));

RemoteFileRequest::RemoteFileRequest(uint32_t id, const QString &file, QObject *parent)
    : QObject(parent), m_file(file), m_id(id)
{
    qDebug() << "Requesting" << file;
}

RemoteFileRequest *RemoteFileRequest::fromNetworkData(const QByteArray& data)
{
    RemoteFileRequest *req = new RemoteFileRequest();
    FileRequestHeader *hdr = (FileRequestHeader*)data.data();
    req->setFile(hdr->fileName);

    return req;
}

QByteArray RemoteFileRequest::toByteArray() const
{
    QByteArray ret(sizeof(FileRequestHeader) + fileName().toUtf8().length() + 1, '\0');
    FileRequestHeader *buf = (FileRequestHeader*)ret.data();
    buf->hdr.request = REQUEST_RECV_FILE;
    strncpy(buf->fileName, fileName().toUtf8().data(), fileName().toUtf8().length() + 1);

    return ret;
}

