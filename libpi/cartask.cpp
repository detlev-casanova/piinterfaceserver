#include "cartask.h"
#include "carhandler.h"
#include "message.h"

#include <stdint.h>
#include <arpa/inet.h>

#include <QByteArray>
#include <QDebug>

typedef uint8_t CarTaskHdrBuf;

#define CAR_TASK_TYPE_REQ	0
#define CAR_TASK_TYPE_RES	1

struct CarTaskReqBuf {
	CarTaskHdrBuf type;
	uint32_t km;
};

struct CarTaskResBuf {
	CarTaskHdrBuf type;
	uint32_t usedTimeDay;
	uint8_t usedKmPc;
	uint8_t usedKm2Pc;
	uint8_t usedTimePc;
};

CarTask::CarTask(TaskManager *parent)
    : Task(parent)
{
	m_startDate = QDateTime(QDate(2016, 6, 28), QTime::currentTime());
	m_today = QDateTime::currentDateTime();
	m_leaseYearCount = 4;
	m_kmPerYear = 40000;
}

int CarTask::process(Message *m)
{
	CarTaskResBuf *buf = (CarTaskResBuf*)m->data().data();

	m_handler->setKmPc1(buf->usedKmPc);
	m_handler->setKmPc2(buf->usedKm2Pc);
	m_handler->setTimePc(buf->usedTimePc);

	m_handler->ready();

	done(this);

	return 0;
}

bool CarTask::isRequest(Message *m)
{
	QByteArray data = m->data();
	CarTaskHdrBuf *hdr = (CarTaskHdrBuf*)(data.data());
	return *hdr == CAR_TASK_TYPE_REQ;
}

bool CarTask::isResponse(Message *m)
{
	QByteArray data = m->data();
	CarTaskHdrBuf *hdr = (CarTaskHdrBuf*)(data.data());
	return *hdr == CAR_TASK_TYPE_RES;
}

bool CarTask::canProcess(Message *m)
{
	return m->type() == CAR_MESSAGE_TYPE && CarTask::isResponse(m);
}

CarHandler *CarTask::request(unsigned int km)
{
	QByteArray data(sizeof(CarTaskReqBuf), 0);
	CarTaskReqBuf *buffer = (CarTaskReqBuf*)data.data();

	buffer->type = CAR_TASK_TYPE_REQ;
	buffer->km = htonl(km);

	Message *m = new Message(CAR_MESSAGE_TYPE, data);
	send(m);

	m_handler = new CarHandler(this);

	return m_handler;
}

void CarTask::reply()
{
	qint64 time_used_s = m_today.toSecsSinceEpoch() - m_startDate.toSecsSinceEpoch();
	qint64 time_s = m_leaseYearCount * 365 * 24 * 60 * 60;

	unsigned int max_km = m_kmPerYear * m_leaseYearCount;

	QByteArray data(sizeof(CarTaskResBuf), 0);
	CarTaskResBuf *buffer = (CarTaskResBuf*)data.data();

	buffer->type = CAR_TASK_TYPE_RES;
	buffer->usedTimePc = (uint8_t)((double)(100 * time_used_s) / time_s);

	buffer->usedKmPc = (100 * m_currentKm) / max_km;
	buffer->usedKm2Pc = (100 * m_currentKm) / (max_km * 1.1);

	Message *m = new Message(CAR_MESSAGE_TYPE, data);
	send(m);
}

CarTask *CarTask::fromNetworkData(const QByteArray &data, TaskManager *taskManager)
{
	CarTaskReqBuf *buf = (CarTaskReqBuf*)data.data();

	if (buf->type != CAR_TASK_TYPE_REQ)
		return NULL;

	CarTask *carTask = new CarTask(taskManager);

	carTask->m_currentKm = ntohl(buf->km);

	return carTask;
}
