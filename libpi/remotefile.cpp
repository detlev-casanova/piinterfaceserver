#include "remotefile.h"

#include <QDir>
#include <QDebug>

#include <arpa/inet.h>

#include "filesystemtask.h"
#include "utils.h"


RemoteFile::RemoteFile(QObject *parent)
    : QObject(parent), m_size(0), m_permissions(0), m_type(0)
{

}

QString RemoteFile::folderName() const
{
    return m_folderName;
}

void RemoteFile::setFolderName(const QString &folderName)
{
    m_folderName = folderName;
}

RemoteFile *RemoteFile::fromNetworkData(const QByteArray &data, int *processed, const QString &folderName)
{
    RemoteFileBuffer *rfBuffer = (RemoteFileBuffer*)data.data();
    RemoteFile *f = new RemoteFile();
    uint16_t fileNameSize = ntohs(rfBuffer->fileNameSize) - 1;

    f->setSize(Utils::ntohll(rfBuffer->size));
    f->setPermissions(ntohl(rfBuffer->permissions));
    f->setType(rfBuffer->type);

    f->setFileName(QString::fromUtf8(rfBuffer->fileName));
    f->setFolderName(folderName);

    if (processed)
        *processed = sizeof(RemoteFileBuffer) + fileNameSize + 1;

    return f;

}

RemoteFile *RemoteFile::fromFileName(const QString &fileName)
{
    qDebug() << "Processing" << fileName;
    RemoteFile *ret = new RemoteFile();
    QFileInfo fileInfo(fileName);

    ret->m_fileName = fileInfo.fileName();
    ret->m_permissions = fileInfo.permissions();
    ret->m_size = fileInfo.size();
    if (fileInfo.isDir())
        ret->m_type = FolderFileType;
    else
        ret->m_type = fileInfo.isSymLink() ? LinkFileType : FileFileType;

    return ret;
}

QByteArray RemoteFile::toByteArray()
{
    QByteArray ret(sizeof(RemoteFileBuffer) + m_fileName.toUtf8().length() + 1, '\0');
    RemoteFileBuffer *buf = (RemoteFileBuffer*)ret.data();
    strncpy(buf->fileName, m_fileName.toUtf8().data(), m_fileName.toUtf8().length() + 1);
    buf->fileNameSize = htons(m_fileName.toUtf8().length() + 1);
    buf->permissions = htonl(m_permissions);
    buf->size = Utils::htonll(m_size);
    buf->type = m_type;

    return ret;
}

void RemoteFile::retrieve(const QString &file)
{

}
