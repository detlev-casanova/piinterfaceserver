#ifndef LOGINSOCKETGENERATOR_H
#define LOGINSOCKETGENERATOR_H

#include <QObject>
#include <QTcpServer>
#include <loginsocket.h>

class LoginSocketGenerator : public QTcpServer
{
	Q_OBJECT
public:
    LoginSocketGenerator(QObject *parent = nullptr);

protected:
    virtual void incomingConnection(qintptr socketDescriptor);

private slots:
	void socketAuthenticated(LoginSocket*);
};

#endif // LOGINSOCKETGENERATOR_H
