#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include "clientsession.h"
#include "loginsocketgenerator.h"
#include <QTcpServer>

// TODO: rename me to ServerSessionManager
class ConnectionManager : public LoginSocketGenerator
{
    Q_OBJECT
public:

    static ConnectionManager * instance();
    bool start();

signals:

public slots:

private slots:
    void createNewSession();
    void processSocketState(QAbstractSocket::SocketState);

private:
    explicit ConnectionManager(QObject *parent = nullptr);

    QList<ClientSession*> m_sessions;
};

#endif // CONNECTIONMANAGER_H
