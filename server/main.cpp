#include <QCoreApplication>

#include "connectionmanager.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ConnectionManager *instance = ConnectionManager::instance();

    if (!instance) {
        qDebug() << "Cannot get connection manager";
        return 1;
    }

    if (!instance->start()) {
        qDebug() << "Cannot start connection manager";
        return 1;
    }

    return a.exec();
}
