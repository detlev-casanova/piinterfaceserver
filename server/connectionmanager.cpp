#include "connectionmanager.h"
#include <QDebug>
#include <QCoreApplication>

static ConnectionManager *__instance = nullptr;

ConnectionManager::ConnectionManager(QObject *parent) : LoginSocketGenerator(parent)
{
    __instance = this;
}

bool ConnectionManager::start()
{
    connect(this, SIGNAL(newConnection()), SLOT(createNewSession()));
    if (!listen(QHostAddress::Any, 4000))
    {
        qDebug() << "Cannot listen on port 4000";
        return false;
    }

    qDebug() << "Server started, listening on " << serverAddress() << ":" << serverPort();

    return true;
}

ConnectionManager * ConnectionManager::instance()
{
    if (!__instance)
        __instance = new ConnectionManager(QCoreApplication::instance());

    return __instance;
}

void ConnectionManager::createNewSession()
{
    if (!hasPendingConnections())
    {
        qDebug() << "No pending connection";
        return;
    }

    LoginSocket * socket = dynamic_cast<LoginSocket*>(nextPendingConnection());
    if (!socket) {
        qDebug() << "No pending connection, It can be already closed or it is not a Login socket";
        return;
    }

    ClientSession * newSession = new ClientSession(socket, this);

    if (!newSession) {
        qDebug() << "Cannot create a new session with the client";
        return;
    }

    connect(newSession, SIGNAL(connectionStateChanged(QAbstractSocket::SocketState)), SLOT(processSocketState(QAbstractSocket::SocketState)));
    connect(newSession, SIGNAL(quit()), QCoreApplication::instance(), SLOT(quit()));

    m_sessions << newSession;

    //if (m_sessions.length() > MAX_SESSION_COUNT)

}

void ConnectionManager::processSocketState(QAbstractSocket::SocketState state)
{
    ClientSession *session;

    if (state != QAbstractSocket::UnconnectedState)
        return;

    session = dynamic_cast<ClientSession*>(sender());

    if (!session)
        return;

    delete session;
}
