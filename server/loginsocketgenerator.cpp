#include "loginsocketgenerator.h"
#include <loginsocket.h>

LoginSocketGenerator::LoginSocketGenerator(QObject *parent)
    : QTcpServer(parent)
{

}

void LoginSocketGenerator::incomingConnection(qintptr socketDescriptor)
{
	LoginSocket *s = new LoginSocket(LoginSocket::LoginSocketServer, this);
    connect(s, SIGNAL(authenticated(LoginSocket*)), SLOT(socketAuthenticated(LoginSocket*)));

    if (!s->setSocketDescriptor(socketDescriptor))
	{
		qDebug() << "Error setting Socket descriptor";
		return;
	}

	// Should connect and do the authentication. Once done (LoginSocket::authenticated()), call addPendingConnection()
	// TODO: manage errors
}

void LoginSocketGenerator::socketAuthenticated(LoginSocket *socket)
{
    addPendingConnection(socket);
}
